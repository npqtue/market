import traceback
import time

from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, create_access_token
from flask_restplus import Namespace, Resource, fields

from app import app, logger
from app.auth import auth_required, admin_required, staff_required
from app.utils import *
from .business import *

api_name = 'Sensors'

api = Namespace(api_name.lower(), description=api_name + ' api')

# >>>>>>>>>>>>>>>>>>>>>>>>>>> MODELS
nested_schema = {}
nested_schema['field1'] = fields.String(required=False)
nested_schema['field2'] = fields.String(required=False)


########################
# Collection structure
# {
#     'name',
#     'ip',    # auto generate at sensor service and write to openvpn ipp.txt
#     'location',
#     'description',
#     'token',  # auto generate at sensor service
#     'ips_rule_change',
#     'ids_rule_change',
#     'create_time',
#     'update_time',
# }
##########################


# Edit below this line
add_model = api.model(
    'Add ' + api_name + ' model',
    {
        'name': fields.String(required=True),
        'location': fields.String(required=True),
        'description': fields.String(required=True)
    }
)

edit_model = api.model(
    'Edit ' + api_name + ' model',
    {
        'name': fields.String(required=False),
        'location': fields.String(required=False),
        'description': fields.String(required=False)
    }
)

# <<<<<<<<<<<<<<<<<<<<<<<<<<< ./END MODELS


@api.route("")
# @admin_required
class Sensors(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(add_model, validate=True)
    def post(self):
        """ Add item """
        try:
            data = escape_request_with_model(request, add_model)

            message = "Create " + api_name.lower()
            is_existed = get_by_field(data['name'], 'name')
            if is_existed:
                message = api_name + " already existed"
                return render_response_template(False, message), 409

            # Default value of sensor
            data['ips_rule_change'] = 0
            data['ids_rule_change'] = 0
            data['create_time'] = time.time()
            data['update_time'] = time.time()

            # Generate sensor_token
            content = {
                'name': data['name']
            }
            post_result = post_to_transport('/sensors', content)
            if not post_result:
                message = message + " fail"
                return render_response_template(False, message), 400

            if 'token' not in post_result or 'ip' not in post_result:
                log_error(post_result)
                message = message + " fail"
                return render_response_template(False, message), 400
            
            data['token'] = post_result['token']
            data['ip'] = post_result['ip']
            data['crt_file'] = post_result['crt_file']
            data['key_file'] = post_result['key_file']
            data['sslcert_file'] = post_result['sslcert_file']
            result = save(data)
            
            if not result:
                message = message + " fail"
                return render_response_template(False, message), 400

            message = message + " success"
            return render_response_template(True, message), 201

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500


    ### API: Get all item
    @api.doc(params={
        'page': 'Paging',
        'limit': 'Limit return records'
    })
    def get(self):
        """ Get all item """
        try:
            page = request.args.get('page', type=int)
            limit = request.args.get('limit', type=int)

            return get_all_paging(page, limit), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500


@api.route("/<string:_id>")
# @admin_required
class SensorsID(Resource):

    @api.doc("abc",params={
    })


    def get(self, _id):
        """
        asfdasldfj
        asdfkjasdf
        asdkfjasd
        """
        try:
            result = get_by_id(_id)
            if not result:
                return render_response_template(False, api_name + ' not found'), 404
            else:
                return result, 200
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500

    
    ### API: Edit item by _id
    @api.doc(params={
    })
    @api.expect(edit_model, validate=True)
    def put(self, _id):
        """ Edit item by _id """
        try:
            data = escape_request_with_model(request, edit_model)
            
            item = get_by_id(_id)
            new_item = None
            if not item:
                return render_response_template(False, api_name + ' not found'), 404

            message = "Update " + api_name.lower()
            if 'location' in data:
                item['location'] = data['location']
            if 'description' in data:
                item['description'] = data['description']
            if 'name' in data and data['name'] != item['name']:
                content = {
                    'name': data['name']
                }
                add_result = Sensors.post(content)
                if not add_result[1] == 201:
                    message = message + " fail"
                    return render_response_template(False, message), 400

                delete_result = self.delete(_id)
                if not delete_result[1] == 200:
                    message = message + " fail"
                    return render_response_template(False, message), 400

                new_item = get_by_field(data['name'], 'name')
                
            result = False
            if new_item:
                item['name'] = new_item['name']
                item['token'] = new_item['token']
                item['ip'] = new_item['ip']
                item['crt_file'] = base64.b64encode(new_item['crt_file'].encode('utf-8'))
                item['key_file'] = base64.b64encode(new_item['key_file'].encode('utf-8'))
                item['sslcert_file'] = base64.b64encode(new_item['sslcert_file'].encode('utf-8'))
                item['update_time'] = time.time()
                save_result = insert(item)
                delete_result = delete_by_id(new_item['_id'])
                if save_result and delete_result:
                    result = True
                else:
                    result = False
            else:
                data['update_time'] = time.time()
                update_result = update_by_id(_id, data)
                if update_result:
                    result = True
                else:
                    result = False

            if not result:
                message = message + " fail"
                return render_response_template(False, message), 400
            else:
                message = message + " success"
                return render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500

    ### API: Delete item by _id
    @api.doc(params={
    })
    def delete(self, _id):
        """ Delete item by _id """
        try:
            message = 'Delete ' + api_name.lower()
            result = get_by_id(_id)
            if not result:
                return render_response_template(False, api_name + ' not found'), 404

            constraint = check_constraint('sensor_id', _id)
            if not constraint:
                message = message + ' violate constraint'
                return render_response_template(False, message), 400
            
            sensor_name = result['name']

            # Revoke certificate of sensor on transport
            delete_result = delete_to_transport('/sensors/' + sensor_name)
            if not delete_result:
                message = message + ' fail'
                return render_response_template(False, message), 400
            
            if not delete_by_id(_id):
                message = message + ' fail'
                return render_response_template(False, message), 400

            message = message + ' success'
            return render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500