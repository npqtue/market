import traceback

from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, create_access_token
from flask_restplus import Namespace, Resource, fields

from app import app, logger, utils
from app.auth import auth_required, admin_required, staff_required
from .business import *
from app.sensors import business as sensors_business

api_name = 'Whitelists'

api = Namespace(api_name.lower(), description=api_name + ' api')

# >>>>>>>>>>>>>>>>>>>>>>>>>>> MODELS
# Nested template model
nested_schema = {}
nested_schema['field1'] = fields.String(required=False)
nested_schema['field2'] = fields.String(required=False)

# Template model
template_model = api.model(
    'Add ' + api_name + ' model',
    {
        'field_string': fields.String(required=True),
        'field_integer': fields.Integer(required=True),
        'field_list_string': fields.List(fields.String, required=True),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=True),
        'field_raw': fields.Raw(required=True)
    }
)

########################
# Collection structure
# {
#     'sensor_id',
#     'alert_name',
#     'endpoint_id',    
#     'src_ip',
#     'src_port',
#     'dest_ip',
#     'dest_port',
#     'proto',
#     'app_proto',
# }
##########################

# Edit below this line
add_model = api.model(
    'Add ' + api_name + ' model',
    {
        'sensor_id': fields.String(required=True),
        'alert_name': fields.String(required=True),
        'endpoint_id': fields.String(required=True),
        'src_ip': fields.String(required=True),
        'src_port': fields.Integer(required=True),
        'dest_ip': fields.String(required=True),
        'dest_port': fields.Integer(required=True),
        'proto': fields.String(required=True),
        'app_proto': fields.String(required=True)
    }
)

edit_model = api.model(
    'Edit ' + api_name + ' model',
    {
        'sensor_id': fields.String(required=False),
        'alert_name': fields.String(required=False),
        'endpoint_id': fields.String(required=False),
        'src_ip': fields.String(required=False),
        'src_port': fields.Integer(required=False),
        'dest_ip': fields.String(required=False),
        'dest_port': fields.Integer(required=False),
        'proto': fields.String(required=False),
        'app_proto': fields.String(required=False)
    }
)

check_model = api.model(
    'Check ' + api_name + ' model',
    {
        'sensor_name': fields.String(required=True),
        'alert_name': fields.String(required=True),
        'endpoint_id': fields.String(required=False),
        'src_ip': fields.String(required=False),
        'src_port': fields.Integer(required=False),
        'dest_ip': fields.String(required=False),
        'dest_port': fields.Integer(required=False),
        'proto': fields.String(required=False),
        'app_proto': fields.String(required=False)
    }
)


# <<<<<<<<<<<<<<<<<<<<<<<<<<< ./END MODELS


@api.route("")
class Whitelists(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(add_model, validate=True)
    def post(self):
        """ Add item """
        try:
            data = utils.escape_request_with_model(request, add_model)

            result = save(data)
            
            message = "Create " + api_name
            if result:
                message = message + " success"
                return utils.render_response_template(True, message), 201
            else:
                message = message + " fail"
                return utils.render_response_template(False, message), 400
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


    ### API: Get all item
    @api.doc(params={
        'page': 'Paging',
        'limit': 'Limit return records'
    })
    def get(self):
        """ Get all item """
        try:
            page = request.args.get('page', type=int)
            limit = request.args.get('limit', type=int)

            return get_all_paging(page, limit), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/<string:_id>")
class WhitelistsID(Resource):
    ### API: Get item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Get item by _id """
        try:
            result = get_by_id(_id)
            if not result:
                return utils.render_response_template(False, api_name + ' not found'), 404
            else:
                return result, 200
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500

    
    ### API: Edit item by _id
    @api.doc(params={
    })
    @api.expect(edit_model, validate=True)
    def put(self, _id):
        """ Edit item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            data = utils.escape_request_with_model(request, edit_model)
            message = "Update " + api_name.lower()
            if not update_by_id(_id, data):
                message = message + " fail"
                return utils.render_response_template(False, message), 400
                
            message = message + " success"
            return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500

    ### API: Delete item by _id
    @api.doc(params={
    })
    def delete(self, _id):
        """ Delete item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            message = 'Delete ' + api_name.lower()
            if not delete_by_id(_id):
                message = message + ' fail'
                return utils.render_response_template(False, message), 400
            else:
                message = message + ' success'
                return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/check")
class WhitelistsCheck(Resource):
    ### API: Check item
    @api.doc(params={
    })
    @api.expect(check_model, validate=True)
    def post(self):
        """ Add item """
        try:
            message = "Check " + api_name
            data = utils.escape_request_with_model(request, check_model)

            sensor = sensors_business.get_by_field(data['sensor_name'], 'name')
            if not sensor:
                return utils.render_response_template(False, 'Sensor not found'), 404
            
            data['sensor_id'] = sensor['_id']
            result = check(data)
            
            if result:
                message = message + " success"
                return utils.render_response_template(True, message), 200
            else:
                message = message + " fail"
                return utils.render_response_template(False, message), 400
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500