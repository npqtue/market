import traceback
import time

from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, create_access_token
from flask_restplus import Namespace, Resource, fields

from app import app, logger
from app.auth import auth_required, admin_required, staff_required
from app import utils
from .business import *
from app.sensors import business as sensors_business
from app.ips.aliases import business as ips_aliases_business

api_name = 'IPS Rules'

api = Namespace('ips/rules', description=api_name + ' api')

# >>>>>>>>>>>>>>>>>>>>>>>>>>> MODELS
# Nested template model
nested_schema = {}
nested_schema['field1'] = fields.String(required=False)
nested_schema['field2'] = fields.String(required=False)

# Template model
template_model = api.model(
    'Add ' + api_name + ' model',
    {
        'field_string': fields.String(required=True),
        'field_integer': fields.Integer(required=True),
        'field_list_string': fields.List(fields.String, required=True),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=True),
        'field_raw': fields.Raw(required=True)
    }
)

########################
# Collection structure
# {
#     'name',
#     'rule_type',          # opnsense, or another type
#     'type',               # pass, block or reject
#     'sensor_id',          # List of string
#     'quick',              # boolean
#     'direction',          # in, out, any
#     'ipprotocol',         # inet, inet6, inet46
#     'protocol',           # tcp, udp, tcp/udp
#     'source_invert',      # boolean
#     'source',
#     'source_port_from',   # integer
#     'source_port_to',     # integer
#     'destination_invert', # boolean
#     'destination',        # alias_id
#     'destination_port_from',      # integer
#     'destination_port_to',        # integer
#     'log',                # boolean
#     'category',
#     'descr',
# }
##########################

# Edit below this line
add_model = api.model(
    'Add ' + api_name + ' model',
    {
        'name': fields.String(required=True),
        'sensor_id': fields.List(fields.String, required=True),
        'rule_type': fields.String(required=True, example="opnsense, or another type"),
        'type': fields.String(required=True, example="pass, block or reject"),
        'quick': fields.Boolean(required=False),
        'direction': fields.String(required=True, example="in, out, any"),
        'ipprotocol': fields.String(required=True, example="inet, inet6, inet46"),
        'protocol': fields.String(required=True, example="tcp, udp, tcp/udp"),
        'source_invert': fields.Boolean(required=False, example=False),
        'source': fields.String(required=False),
        'source_port_from': fields.Integer(required=False),
        'source_port_to': fields.Integer(required=False),
        'destination_invert': fields.Boolean(required=False, example=False),
        'destination': fields.String(required=False),
        'destination_port_from': fields.Integer(required=False),
        'destination_port_to': fields.Integer(required=False),
        'log': fields.Boolean(required=False, example=False),
        'category': fields.String(required=False),
        'descr': fields.String(required=False, example="description")
    }
)

edit_model = api.model(
    'Edit ' + api_name + ' model',
    {
        'name': fields.String(required=False),
        'sensor_id': fields.List(fields.String, required=False),
        'rule_type': fields.String(required=False, example="opnsense, or another type"),
        'type': fields.String(required=False, example="pass, block or reject"),
        'quick': fields.Boolean(required=False),
        'direction': fields.String(required=False, example="in, out, any"),
        'ipprotocol': fields.String(required=False, example="inet, inet6, inet46"),
        'protocol': fields.String(required=False, example="tcp, udp, tcp/udp"),
        'source_invert': fields.Boolean(required=False, example=False),
        'source': fields.String(required=False),
        'source_port_from': fields.Integer(required=False),
        'source_port_to': fields.Integer(required=False),
        'destination_invert': fields.Boolean(required=False, example=False),
        'destination': fields.String(required=False),
        'destination_port_from': fields.Integer(required=False),
        'destination_port_to': fields.Integer(required=False),
        'log': fields.Boolean(required=False, example=False),
        'category': fields.String(required=False),
        'descr': fields.String(required=False, example="description")
    }
)

# <<<<<<<<<<<<<<<<<<<<<<<<<<< ./END MODELS


@api.route("")
# @admin_required
class IPSRules(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(add_model, validate=True)
    def post(self):
        """ Add item """
        try:
            data = utils.escape_request_with_model(request, add_model)
            # Process data to avoid misstake from frontend
            data['statetype'] = 'keep state'
            data['floating'] = 'yes'
            quick = 'no'
            if 'quick' in data and data['quick'] == True:
                quick = 'yes'
            data['quick'] = quick

            log = 0
            if 'log' in data and data['log'] == True:
                log = 1
            data['log'] = log

            # source transform
            source_invert = 'no'
            if 'source_invert' in data and data['source_invert'] == True:
                source_invert = 'yes'
            data['source_invert'] = source_invert

            if 'source' not in data or data['source'] == '' or data['source'] == 'any':
                data['source'] = {'any': 1}
            else:
                data['source'] = {'address': data['source'].replace('/32', '')}

            if 'source_port_from' in data and data['source_port_from'] > 65536:
                message = "source_port_from must less than 65536"
                return utils.render_response_template(False, message), 400
            if 'source_port_from' not in data or data['source_port_from'] == 0:
                data['source_port_from'] = 'any'

            if 'source_port_to' in data and data['source_port_to'] > 65536:
                message = "source_port_to must less than 65536"
                return utils.render_response_template(False, message), 400
            if 'source_port_to' not in data or data['source_port_to'] == 0:
                data['source_port_to'] = 'any'
            
            if data['source_port_from'] != 'any' and data['source_port_to'] != 'any':
                if data['source_port_from'] == data['source_port_to']:
                    data['source']['port'] = data['source_port_from']
                else:
                    data['source']['port'] = str(data['source_port_from']) + '-' + str(data['source_port_to'])
            del(data['source_port_from'])
            del(data['source_port_to'])

            # destination transform
            destination_invert = 'no'
            if 'destination_invert' in data and data['destination_invert'] == 'y':
                destination_invert = 'yes'
            data['destination_invert'] = destination_invert

            if 'destination' not in data or data['destination'] == '':
                data['destination'] = {'any': 1}
            else:
                is_alias_exist = ips_aliases_business.get_by_id(data['destination'])
                if not is_alias_exist:
                    message = "Destination mapping to alias_id not found"
                    return utils.render_response_template(False, message), 400
                data['destination'] = {'ips_alias_id': data['destination']}

            if 'destination_port_from' in data and data['destination_port_from'] > 65536:
                message = "destination_port_from must less than 65536"
                return utils.render_response_template(False, message), 400
            if 'destination_port_from' not in data or data['destination_port_from'] == 0:
                data['destination_port_from'] = 'any'

            if 'destination_port_to' in data and data['destination_port_to'] > 65536:
                message = "destination_port_to must less than 65536"
                return utils.render_response_template(False, message), 400
            if 'destination_port_to' not in data or data['destination_port_to'] == '' or data['destination_port_to'] == 0:
                data['destination_port_from'] = 'any'
            
            if data['destination_port_from'] != 'any' and data['destination_port_to'] != 'any':
                if data['destination_port_from'] == data['destination_port_to']:
                    data['destination']['port'] = data['destination_port_from']
                else:
                    data['destination']['port'] = str(data['destination_port_from']) + '-' + str(data['destination_port_to'])
            del(data['destination_port_from'])
            del(data['destination_port_to'])

            # another default properties if empty
            if 'category' not in data:
                data['category'] = None
            if 'descr' not in data:
                data['descr'] = data['name']
            data['updated'] = {
                'username': 'SOC',
                'time': time.time(),
                'description': 'SOC made changes'
            }
            data['created'] = {
                'username': 'SOC',
                'time': time.time(),
                'description': 'SOC made changes'
            }

            result = save(data)
            
            message = "Create " + api_name
            if result:
                for sensor_id in data['sensor_id']:
                    sensors_business.update_ips_rule_change(sensor_id, 1)
                message = message + " success"
                return utils.render_response_template(True, message), 201
            else:
                log_error('Error: ' + str(result))
                message = message + " fail"
                return utils.render_response_template(False, message), 400
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500


    ### API: Get all item
    @api.doc(params={
        'page': 'Paging',
        'limit': 'Limit return records'
    })
    def get(self):
        """ Get all item """
        try:
            page = request.args.get('page', type=int)
            limit = request.args.get('limit', type=int)

            return get_all_paging(page, limit), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500


@api.route("/<string:_id>")
# @admin_required
class IPSRulesID(Resource):
    ### API: Get item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Get item by _id """
        try:
            result = get_by_id(_id)
            if not result:
                return utils.render_response_template(False, api_name + ' not found'), 404
            else:
                fulfill_data(result)
                return result, 200
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500

    
    ### API: Edit item by _id
    @api.doc(params={
    })
    @api.expect(edit_model, validate=True)
    def put(self, _id):
        """ Edit item by _id """
        try:
            item = get_by_id(_id)
            if not get_by_id(_id):
                return utils.render_response_template(False, api_name + ' not found'), 404

            data = utils.escape_request_with_model(request, edit_model)
            # Process data to avoid misstake from frontend
            if 'quick' in data:
                if data['quick'] == True:
                    quick = 'yes'
                else:
                    quick = 'no'
                data['quick'] = quick

            if 'log' in data:
                if data['log'] == True:
                    log = 1
                else:
                    log = 0
                data['log'] = log

            # source transform
            if 'source_invert' in data:
                if data['source_invert'] == True:
                    source_invert = 'yes'
                else:
                    source_invert = 'no'
                data['source_invert'] = source_invert

            if 'source' in data:
                if data['source'] == '' or data['source'] == 'any':
                    data['source'] = {'any': 1}
                else:
                    data['source'] = {'address': data['source'].replace('/32', '')}

            is_change_port = True
            if 'source_port_from' in data:
                is_change_port = not is_change_port
                if data['source_port_from'] > 65536 or data['source_port_from'] < 0:
                    message = "source_port_from must less than 65536 and greater or equal than 0"
                    return utils.render_response_template(False, message), 400
                elif data['source_port_from'] == 0:
                    data['source_port_from'] = 'any'
            
            if 'source_port_to' in data:
                is_change_port = not is_change_port
                if data['source_port_to'] > 65536 or data['source_port_to'] < 0:
                    message = "source_port_to must less than 65536 and greater or equal than 0"
                    return utils.render_response_template(False, message), 400
                elif data['source_port_to'] == 0:
                    data['source_port_to'] = 'any'
            
            if not is_change_port:
                message = "source_port_from and source_port_to require for change port"
                return utils.render_response_template(False, message), 400

            if 'source_port_from' in data and 'source_port_to' in data:
                if data['source_port_from'] != 'any' and data['source_port_to'] != 'any':
                    if data['source_port_from'] == data['source_port_to']:
                        data['source']['port'] = data['source_port_from']
                    else:
                        data['source']['port'] = str(data['source_port_from']) + '-' + str(data['source_port_to'])
                del(data['source_port_from'])
                del(data['source_port_to'])

            # destination transform
            if 'destination_invert' in data:
                if data['destination_invert'] == True:
                    destination_invert = 'yes'
                else:
                    destination_invert = 'no'
                data['destination_invert'] = destination_invert

            if 'destination' in data:
                if data['destination'] == '' or data['destination'] == 'any':
                    data['destination'] = {'any': 1}
                else:
                    is_alias_exist = ips_aliases_business.get_by_id(data['destination'])
                    if not is_alias_exist:
                        message = "Destination mapping to alias_id not found"
                        return utils.render_response_template(False, message), 400
                    data['destination'] = {'ips_alias_id': data['destination']}

            is_change_port = True
            if 'destination_port_from' in data:
                is_change_port = not is_change_port
                if data['destination_port_from'] > 65536 or data['destination_port_from'] < 0:
                    message = "destination_port_from must less than 65536 and greater or equal than 0"
                    return utils.render_response_template(False, message), 400
                elif data['destination_port_from'] == 0:
                    data['destination_port_from'] = 'any'
            
            if 'destination_port_to' in data:
                is_change_port = not is_change_port
                if data['destination_port_to'] > 65536 or data['destination_port_to'] < 0:
                    message = "destination_port_to must less than 65536 and greater or equal than 0"
                    return utils.render_response_template(False, message), 400
                elif data['destination_port_to'] == 0:
                    data['destination_port_to'] = 'any'

            if not is_change_port:
                message = "destination_port_from and destination_port_to require for change port"
                return utils.render_response_template(False, message), 400

            if 'destination_port_from' in data and 'destination_port_to' in data:
                if data['destination_port_from'] != 'any' and data['destination_port_to'] != 'any':
                    if data['destination_port_from'] == data['destination_port_to']:
                        data['destination']['port'] = data['destination_port_from']
                    else:
                        data['destination']['port'] = str(data['destination_port_from']) + '-' + str(data['destination_port_to'])
                del(data['destination_port_from'])
                del(data['destination_port_to'])
            message = "Update " + api_name.lower()

            list_change_sensor_id = set(item['sensor_id'])
            if 'sensor_id' in data:
                if set(data['sensor_id']) != set(item['sensor_id']):
                    list_change_sensor_id = set(data['sensor_id']) | set(item['sensor_id'])

            if not update_by_id(_id, data):
                message = message + " fail"
                return utils.render_response_template(False, message), 400
            else:
                for sensor_id in list_change_sensor_id:
                    sensors_business.update_ips_rule_change(sensor_id, 1)
                message = message + " success"
                return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500

    ### API: Delete item by _id
    @api.doc(params={
    })
    def delete(self, _id):
        """ Delete item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            message = 'Delete ' + api_name.lower()
            if not delete_by_id(_id):
                message = message + ' fail'
                return utils.render_response_template(False, message), 400
            else:
                for sensor_id in item['sensor_id']:
                    sensors_business.update_ips_rule_change(sensor_id, 1)
                message = message + ' success'
                return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500


@api.route("/is_change")
class IPSRuleIsChange(Resource):
    ### API: Check is IPS rules changed
    @api.doc(params={
    })
    def get(self):
        """ Check is IPS rules changed """
        try:
            is_change = sensors_business.get_by_ips_rule_change(1)
            if not is_change:
                message = "No IPS rule changed"
                return utils.render_response_template(False, message), 404
            
            message = 'IPS rules changed'
            return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/apply")
class IPSRuleApply(Resource):
    ### API: Apply ips rule to sensors
    @api.doc(params={
    })
    def get(self):
        """ Apply ips rule to sensors """
        try:
            message = "Apply IPS rule to sensor"
            list_change_sensor = sensors_business.get_by_ips_rule_change(1)
            if not list_change_sensor:
                message = "No IPS rule on sensor changed"
                return utils.render_response_template(False, message), 400
                
            result = True
            fail_sensor = []
            for sensor in list_change_sensor:
                list_ips_rule = get_by_sensor_id(sensor['_id'])
                list_ips_alias = []
                for ips_rule in list_ips_rule:
                    if 'ips_alias_id' in ips_rule['destination']:
                        ips_alias = ips_aliases_business.get_by_id(ips_rule['destination']['ips_alias_id'])
                        if not ips_alias:
                            message = "Data is not consistency, please check ips_alias and ips_rules collections"
                            return utils.render_response_template(False, message), 400
                        list_ips_alias.append(ips_alias)
                        del(ips_rule['destination']['ips_alias_id'])
                        ips_rule['destination']['address'] = ips_alias['name']
                        
                content = {}
                content['sensor_ip'] = sensor['ip']
                content['action'] = 'post'
                content['url'] = '/ips'
                content['body'] = {}
                content['body']['ips_rules'] = list_ips_rule
                if list_ips_alias:
                    content['body']['ips_aliases'] = list_ips_alias

                post_result = post_to_transport('/sensors/action', content)
                if not post_result:
                    result = False
                    fail_sensor.append(sensor['name'])
                else:
                    sensors_business.update_ips_rule_change(sensor['_id'], 0)

            if not result:
                message = message + " fail on " + str(fail_sensor)
                return utils.render_response_template(False, message), 400
            
            message = message + " success"
            return utils.render_response_template(True, message), 200
                
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500