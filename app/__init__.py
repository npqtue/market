# -*- coding: utf-8 -*-
# @Author: Toor
# @Date: 2019-05-03 10:47:49
# @Last Modified by:   Toor
# @Last Modified time: 2019-05-03 10:47:49

from flask import Flask, Blueprint
from flask_restplus import Api
import logging
from logging import basicConfig, DEBUG, getLogger, StreamHandler
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)
from jwt import (
    ExpiredSignatureError,
    InvalidTokenError,
    DecodeError,
    InvalidIssuerError
)
from flask_jwt_extended.exceptions import (
    JWTDecodeError,
    NoAuthorizationError
)

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

import certifi
import config

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = config.Config.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db)
mongo = None
es = None
rabbitmq = None
redis = None
logger = None
jwt = JWTManager()
v1_blueprint = Blueprint('api', __name__)

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

v1_api = Api(
    v1_blueprint,
    title=config.Config.APP_NAME + ' API',
    version='1.0',
    description=config.Config.APP_NAME + ' API',
    authorizations=authorizations, security='apikey'
)


def api_errors_handlers(api):

    @api.errorhandler(ExpiredSignatureError)
    def handle_expired_signature_error(error):
        return {'message': 'Token expired'}, 401

    @api.errorhandler(NoAuthorizationError)
    def handle_no_authorization_error(error):
        return {'message': 'Authorization Header required'}, 403

    @api.errorhandler(InvalidTokenError)
    @api.errorhandler(DecodeError)
    @api.errorhandler(InvalidIssuerError)
    def handle_invalid_token_error(error):
        return {'message': 'Token incorrect, supplied or malformed'}, 401


def configure_log(app):
    global logger
    logger = logging.getLogger(__name__)
    logging.basicConfig(
        filename='log/error.log',
        level=logging.DEBUG,
        format="%(asctime)s|%(name)-20.20s|%(levelname)-5.5s|%(message)s"
    )

    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("pika").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)


def create_app(config):
    global app
    app.config.from_object(config)

    if app.config['USING_MONGO'] is True:
        from flask_pymongo import PyMongo
        global mongo
        mongo = PyMongo(app)

    if app.config['USING_ES'] is True:
        global es
        from elasticsearch import Elasticsearch
        es = Elasticsearch(app.config['ES_HOST'],
                           http_auth=(app.config['ES_USER'],
                                      app.config['ES_PASSWORD']),
                           scheme=app.config['ES_SCHEME'],
                           port=app.config['ES_PORT'],
                           ca_certs=certifi.where()
                           )

    if app.config['USING_RABBITMQ'] is True:
        global rabbitmq
        import pika
        credentials = pika.PlainCredentials(
            app.config['QUEUE_USER'],
            app.config['QUEUE_PASSWORD']
        )
        queue = pika.BlockingConnection(pika.ConnectionParameters(
            host=app.config['QUEUE_HOST'],
            virtual_host=app.config['QUEUE_VHOST'],
            credentials=credentials
        ))
        rabbitmq = queue.channel()
        # Make durable queue
        rabbitmq.queue_declare(queue=app.config['QUEUE_NAME'], durable=True)

    if app.config['USING_REDIS'] is True:
        global redis
        import redis as Redis
        redis = Redis.StrictRedis(
            host=app.config['REDIS_HOST'],
            port=app.config['REDIS_PORT']
        )

    global jwt
    jwt = JWTManager(app)
    configure_log(app)

    api_errors_handlers(v1_api)

    # Namespaces register
    from .sensors import api as sensors_ns
    from .endpoints import api as endpoints_ns
    from .ids import api as ids_ns
    from .ips.aliases import api as ips_aliases_ns
    from .ips.rules import api as ips_rules_ns
    from .nac.switches import api as nac_switches_ns
    from .identities import api as identities_ns
    from .subnets import api as subnets_ns
    from .alerts import api as alerts_ns
    from .whitelists import api as whitelists_ns
    from .users.routes import api as users_ns

    v1_api.add_namespace(sensors_ns)
    v1_api.add_namespace(endpoints_ns)
    v1_api.add_namespace(ids_ns)
    v1_api.add_namespace(ips_aliases_ns)
    v1_api.add_namespace(ips_rules_ns)
    v1_api.add_namespace(nac_switches_ns)
    v1_api.add_namespace(identities_ns)
    v1_api.add_namespace(subnets_ns)
    v1_api.add_namespace(alerts_ns)
    v1_api.add_namespace(whitelists_ns)
    v1_api.add_namespace(users_ns)

    return app
