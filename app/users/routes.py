from bcrypt import checkpw, gensalt, hashpw
from flask import jsonify, json, render_template, redirect, request, url_for, abort
from flask_login import (
    current_user,
    login_required,
    login_user,
    logout_user
)

from app import app, db
# from app.utils.need_permission import NeedPermission as need_permission
from app.users.forms import LoginForm, CreateUserForm
from app.users.models import User
# from app.sensors.models import Sensor
# from app.conf.models import Department
from flask_expects_json import expects_json
from sqlalchemy import or_
import random

from pprint import pprint

from flask_restplus import Namespace, Resource, fields


api_name = 'Users'

api = Namespace(api_name.lower(), description=api_name + ' api')
# -------------------------------------------------------------+
#                         USER                                |
# -------------------------------------------------------------+

# @blueprint.route('/', methods=['GET'])
# @login_required
# def users_get_all():
#     """
#     List all users
#     """
#     # need_permission('eq', 'admin')

#     output = []
#     query = User.query.filter(User.id != 1).join(
#         Sensor, Sensor.id == User.sensor_id).add_columns(Sensor.name).all()

#     for (user, sensor) in query:
#         output.append({
#             "id": user.id,
#             "username": user.username,
#             "fullname": user.fullname,
#             "email": user.email,
#             "role": user.role,
#             "departments": user.departments,
#             "sensor": sensor,
#             "teams": user.teams,
#             "status": user.status,
#             "notify": user.notify,
#             "media": user.media,
#             "description": user.description
#         })
#     return jsonify(output)


@blueprint.route('/', methods=['POST'])
@login_required
def user_create():
    """
    Add new user
    """
    # need_permission('eq', 'admin')

    user = {}
    user['username'] = request.form['username'].strip()
    user['username_hash'] = request.form['username_hash']
    user['password'] = request.form['password']
    user['fullname'] = request.form['fullname']
    user['email'] = request.form['email']
    user['description'] = request.form['description']
    user['role'] = request.form['role']
    user['sensor_id'] = int(request.form['sensor'])

    try:
        json.loads(request.form['media'])
        user['media'] = request.form['media']
    except ValueError:
        return jsonify('error')

    try:
        request.form['notify']
        user['notify'] = 1
    except KeyError:
        user['notify'] = 0

    user['departments'] = ','.join(request.form.getlist('departments'))
    user['teams'] = ','.join(request.form.getlist('teams'))

    _user = User(user)
    db.session.add(_user)

    try:
        db.session.commit()
        return jsonify('success')
    except Exception as e:
        db.session.rollback()
        db.session.flush()
        return jsonify('error')


# @blueprint.route('/', methods=['PUT'])
# @login_required
# def user_update():
#     """
#     Update user
#     """
#     # need_permission('eq', 'admin')

#     user_id = request.form['id']
#     if user_id == '1':
#         abort(404)

#     user = User.query.filter_by(id=user_id).first()

#     user.fullname = request.form['fullname']
#     user.email = request.form['email']
#     user.description = request.form['description']

#     user.notify = 1 if request.form['notify'] == 'on' else 0
#     user.departments = ','.join(request.form.getlist('departments'))
#     user.teams = ','.join(request.form.getlist('teams'))

#     user.sensor_id = int(request.form['sensor'])

#     if request.form['role'] not in ['su', 'se', 'admin']:
#         return jsonify('error')
#     else:
#         user.role = request.form['role']

#     try:
#         json.loads(request.form['media'])
#         user.media = request.form['media']
#     except ValueError:
#         return jsonify('error')

#     if request.form['password'] != '':
#         user.password = hashpw(
#             request.form['password'].strip().encode('utf8'), gensalt())

#     try:
#         db.session.commit()
#         return jsonify('success')
#     except Exception as e:
#         db.session.rollback()
#         db.session.flush()
#         return jsonify('error')


# @blueprint.route('/<id>', methods=['DELETE'])
# @login_required
# def user_delete(id):
#     """
#     Delete a sensor
#     """
#     # need_permission('eq', 'admin')

#     # prevent ADMIN delete
#     if id == '1':
#         abort(404)

#     User.query.filter_by(id=id).delete()

#     try:
#         db.session.commit()
#         return jsonify('success')
#     except Exception as e:
#         db.session.rollback()
#         db.session.flush()
#         return jsonify('error')


# @blueprint.route('/login', methods=['GET', 'POST'])
# def login():
#     """
#     Login
#     """
#     login_form = LoginForm(request.form)

#     if request.method == 'GET' and not current_user.is_authenticated:
#         session_pictures = app.config['LOGIN_PICTURES'][random.randrange(
#             len(app.config['LOGIN_PICTURES']))]
#         return render_template('login.html', login_form=login_form, picture=session_pictures)

#     if request.method == 'POST' and login_form.validate_on_submit():
#         username = request.form['username']
#         password = request.form['password']
#         user = User.query.filter_by(username_hash=username).first()

#         if user and checkpw(password.encode('utf8'), user.password):
#             # if user:
#             login_user(user)
#             return jsonify({'code': 'success', 'next': '/home/index'})
#         else:
#             return jsonify({'code': 'error', 'next': ''})

#     return redirect(url_for('home_blueprint.index'))


# @blueprint.route('/logout')
# @login_required
# def logout():
#     """
#     Logout
#     """
    # logout_user()
#     return redirect(url_for('users_blueprint.login'))


# @blueprint.route('/profile/<int:user_id>', methods=['GET'])
# @login_required
# def user_profile(user_id):
#     """
#     User profile page
#     """

#     #!!!!!!!!! Prevent wrong user edit
#     if current_user.id != user_id:
#         abort(400)
#     #!!!!!!!!! Prevent wrong user edit

#     query = User.query.filter(User.id == user_id).join(
#         Sensor, Sensor.id == User.sensor_id).add_columns(Sensor.name).first()

#     if not query:
#         abort(400)

#     user, sensor_name = query

#     result = {}

#     # role
#     if user.role == 'su':
#         role = "SOC User"
#     elif user.role == 'se':
#         role = "SOC Engineer"
#     elif user.role == 'admin':
#         role = "SOC Administrator"
#     else:
#         role = "UNKNOWN"

#     # status
#     status = 'Active' if user.status else 'Disable'

#     result['role'] = role
#     result['username'] = user.username
#     result['fullname'] = user.fullname
#     result['teams'] = user.teams
#     result['status'] = status
#     result['user_id'] = user.id
#     result['email'] = user.email
#     result['sensor'] = sensor_name

#     return render_template('user_profile.html', user=result)


# @blueprint.route('/profile/<int:user_id>/changepassword', methods=['POST'])
# @login_required
# def user_change_password(user_id):
#     """
#     User change password
#     """

#     #!!!!!!!!! Prevent wrong user edit
#     if current_user.id != user_id:
#         abort(400)
#     #!!!!!!!!! Prevent wrong user edit

#     data = dict(request.form)

#     try:
#         _user_id = data['user_id']
#         current_pwd = data['c_pwd']
#         new_pwd = data['n_pwd']
#         new_pwd_again = data['a_pwd']
#     except Exception as e:
#         abort(400)

#     if current_user.id != int(_user_id):
#         abort(400)

#     if new_pwd != new_pwd_again:
#         abort(400)

#     user_query = User.query.filter_by(id=int(_user_id)).first()

#     if user_query and checkpw(current_pwd.encode('utf8'), user_query.password):
#         user_query.password = hashpw(new_pwd.strip().encode('utf8'), gensalt())

#         try:
#             db.session.commit()
#             return jsonify('success')
#         except Exception as e:
#             db.session.rollback()
#             db.session.flush()
#             return jsonify('error')

#     return jsonify('error')


# ###################################################################################################################
# department_schema = {
#     'sensor_id': 'string',
#     'required': ['alert_department']
# }


# @blueprint.route('/get_notify_info', methods=['POST'])
# # @expects_json(department_schema)
# def get_notify_info():
#     data = request.json
#     sensor_id = None
#     department = None
#     team = None
#     notify_info = []
#     if 'sensor_id' in data:
#         sensor_id = data['sensor_id']
#     if 'name' in data and data['name'] != '':
#         department = data['name']
#     if 'team' in data and data['team'] != '':
#         team = data['team']

#     # Get all user with team as alert team
#     if team is not None:
#         users = User.query.filter_by(sensor_id=sensor_id, notify=True).filter(
#             User.teams.like("%" + team + "%")).all()
#         if len(users) > 0:
#             for user in users:
#                 team_verify = user.teams.split(',')
#                 if team in team_verify:
#                     notify_info.append(user.media)

#     # Get all user with deparment as alert department
#     if department is not None:
#         users = User.query.filter_by(sensor_id=sensor_id, teams='', notify=True).filter(
#             User.departments.like("%" + department + "%")).all()
#         if len(users) > 0:
#             for user in users:
#                 department_verify = user.departments.split(',')
#                 if department in department_verify:
#                     notify_info.append(user.media)

#     # Get all user with admin rights
#     users = User.query.filter(
#         or_(User.role == 'se', User.role == 'admin')).filter_by(notify=True)
#     if users.count() > 0:
#         for user in users:
#             notify_info.append(user.media)

#     return jsonify(notify_info), 200
