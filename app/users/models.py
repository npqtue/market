from bcrypt import gensalt, hashpw
from flask_login import UserMixin
# from app import db, login_manager
from datetime import datetime
from app import db


class User(db.Model, UserMixin):

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True, nullable=False)
    username = db.Column(db.String(60), index=True,
                         unique=True, nullable=False)
    username_hash = db.Column(db.String(200), nullable=False)
    fullname = db.Column(db.String(60), index=True, nullable=False)
    password = db.Column(db.Binary, nullable=False)
    departments = db.Column(db.String(255), nullable=True)
    teams = db.Column(db.String(255), nullable=True)
    role = db.Column(db.String(120), nullable=False)
    status = db.Column(db.Integer, default=1)
    is_admin = db.Column(db.Integer)
    notify = db.Column(db.Boolean, default=False)
    media = db.Column(db.String(500), nullable=True)
    create_time = db.Column(db.DateTime, default=datetime.utcnow)
    update_time = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    description = db.Column(db.String(255), nullable=False)

    def __init__(self, data):
        for property, value in data.items():
            if hasattr(value, '__iter__') and not isinstance(value, str):
                value = value[0]
            if property == 'password':
                value = hashpw(value.encode('utf8'), gensalt())
            setattr(self, property, value)

    def __repr__(self):
        return str(self.username)


# @login_manager.user_loader
# def user_loader(id):
#     return User.query.filter_by(id=id).first()


# @login_manager.request_loader
# def request_loader(request):
#     username = request.form.get('username')
#     user = User.query.filter_by(username=username).first()
#     return user if user else None
