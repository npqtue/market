#!/bin/bash
if [ $# -ne 1 ]; then
    echo "Usage: $0 <prod|dev|local>"
    exit 1
fi
pip3 install -r requirements.txt > /dev/null
env=$1
worker=5
host="0.0.0.0"
port="8080"
access_log="log/access.log"
error_log="log/error.log"
log_level="error"
options=""

# Turn on reload code 
export FLASK_CONFIG=$env
if [[ $env != "prod" ]]; then
    export FLASK_DEBUG=True
    options="$options --reload"
    options="$options --access-logfile=-"
    options="$options --error-logfile=-"
    options="$options -w 1"
else
    export FLASK_DEBUG=True
    # Write access_log to file
    options="$options --access-logfile=-"
    options="$options --error-logfile $error_log"
    options="$options -w $worker"
fi

# access_log format as default
log_format=' --access-logformat '\''%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'\'
#options="$options $log_format"

# log level
options="$options --log-level $log_level"


# bind host and port
options="$options -b $host:$port"

# run app
hash gunicorn
if [[ $? != 0 ]]; then
    pip3 install gunicorn
fi
echo $options
gunicorn $options wsgi:app
