import mysql.connector
import requests, json

mysql_server = '127.0.0.1'
mysql_db = 'soc'
mysql_user = 'root'

connection = mysql.connector.connect(
    host = mysql_server, 
    database = mysql_db,
    user = mysql_user
)

cursor = connection.cursor()
query = 'select name, team, vlan, description from department'
cursor.execute(query)
departments = cursor.fetchall()

for department in departments:
    content = {}
    content['sensor_id'] = '5d53750d111e350db78385ba'
    content['department'] = department[0]
    content['unit'] = department[1]
    content['team'] = ''
    content['subnet'] = department[2]
    content['description'] = department[3]

    response = requests.post(
        'http://172.16.103.78:8080/api/v1/subnets',
        data=json.dumps(content),
        headers={
            'Content-Type': 'application/json'
        }
    )
    if response.status_code != 200:
        print(response.content)