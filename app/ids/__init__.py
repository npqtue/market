import traceback

from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, create_access_token
from flask_restplus import Namespace, Resource, fields

from app import app, logger
from app.auth import auth_required, admin_required, staff_required
from app import utils
from .business import *
from app.sensors import business as sensors_business

api_name = 'IDS'

api = Namespace(api_name.lower(), description=api_name + ' api')

# >>>>>>>>>>>>>>>>>>>>>>>>>>> MODELS
# Nested template model
nested_schema = {}
nested_schema['field1'] = fields.String(required=False)
nested_schema['field2'] = fields.String(required=False)

# Template model
template_model = api.model(
    'Add ' + api_name + ' model',
    {
        'field_string': fields.String(required=True),
        'field_integer': fields.Integer(required=True),
        'field_list_string': fields.List(fields.String, required=True),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=True),
        'field_raw': fields.Raw(required=True)
    }
)

########################
# Collection structure
# {
#     'name',
#     'type', #suricata or some ids type
#     'rule_id',    
#     'status',    
#     'sensor_id',
#     'raw_content',
#     'description',  
# }
##########################

# Edit below this line
add_model = api.model(
    'Add ' + api_name + ' model',
    {
        'name': fields.String(required=True),
        'type': fields.String(required=True),
        'rule_id': fields.Integer(required=True),
        'sensor_id': fields.List(fields.String, required=True),
        'raw_content': fields.String(required=True),
        'description': fields.String(required=True)
    }
)

edit_model = api.model(
    'Edit ' + api_name + ' model',
    {
        'name': fields.String(required=False),
        'type': fields.String(required=False),
        'rule_id': fields.Integer(required=False),
        'sensor_id': fields.List(fields.String, required=False),
        'raw_content': fields.String(required=False),
        'description': fields.String(required=False)
    }
)

disable_model = api.model(
    'Disable ' + api_name + ' model',
    {
        'name': fields.String(required=True),
        'type': fields.String(required=True),
        'rule_id': fields.Integer(required=True),
        'sensor_id': fields.List(fields.String, required=True),
        'description': fields.String(required=True)
    }
)

# <<<<<<<<<<<<<<<<<<<<<<<<<<< ./END MODELS


@api.route("")
# @admin_required
class IDS(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(add_model, validate=True)
    def post(self):
        """ Add item """
        try:
            data = utils.escape_request_with_model(request, add_model)

            data['status'] = 'enable'
            result = save_unique(data, 'rule_id')
            
            message = "Create " + api_name
            if result:
                for sensor_id in data['sensor_id']:
                    sensors_business.update_ids_rule_change(sensor_id, 1)
                message = message + " success"
                return utils.render_response_template(True, message), 201
            else:
                message = message + " fail"
                return utils.render_response_template(False, message), 409
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500


    ### API: Get all item
    @api.doc(params={
        'page': 'Paging',
        'limit': 'Limit return records'
    })
    def get(self):
        """ Get all item """
        try:
            page = request.args.get('page', type=int)
            limit = request.args.get('limit', type=int)

            return get_all_paging(page, limit), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500


@api.route("/<string:_id>")
# @admin_required
class IDSID(Resource):
    ### API: Get item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Get item by _id """
        try:
            result = get_by_id(_id)
            if not result:
                return utils.render_response_template(False, api_name + ' not found'), 404
            else:
                return result, 200
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500

    
    ### API: Edit item by _id
    @api.doc(params={
    })
    @api.expect(edit_model, validate=True)
    def put(self, _id):
        """ Edit item by _id """
        try:
            item = get_by_id(_id)
            if not get_by_id(_id):
                return utils.render_response_template(False, api_name + ' not found'), 404

            data = utils.escape_request_with_model(request, edit_model)
            message = "Update " + api_name.lower()

            list_change_sensor_id = set(item['sensor_id'])
            if 'sensor_id' in data:
                if set(data['sensor_id']) != set(item['sensor_id']):
                    list_change_sensor_id = set(data['sensor_id']) | set(item['sensor_id'])

            if not update_by_id(_id, data):
                message = message + " fail"
                return utils.render_response_template(False, message), 400
            else:
                for sensor_id in list_change_sensor_id:
                    sensors_business.update_ids_rule_change(sensor_id, 1)
                message = message + " success"
                return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500

    ### API: Delete item by _id
    @api.doc(params={
    })
    def delete(self, _id):
        """ Delete item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            message = 'Delete ' + api_name.lower()
            if not delete_by_id(_id):
                message = message + ' fail'
                return utils.render_response_template(False, message), 400
            else:
                for sensor_id in item['sensor_id']:
                    sensors_business.update_ids_rule_change(sensor_id, 1)
                message = message + ' success'
                return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500


@api.route("/is_change")
class IDSRuleIsChange(Resource):
    ### API: Check is IDS rules changed
    @api.doc(params={
    })
    def get(self):
        """ Check is IDS rules changed """
        try:
            is_change = sensors_business.get_by_ids_rule_change(1)
            if not is_change:
                message = "No IDS rule changed"
                return utils.render_response_template(False, message), 404
            
            message = 'IDS rules changed'
            return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/apply")
# @admin_required
class IDSApply(Resource):
    ### API: Apply ids rule to sensors
    @api.doc(params={
    })
    def get(self):
        """ Apply ids rule to sensors """
        try:
            message = "Apply IDS rule to sensor"
            list_change_sensor = sensors_business.get_by_ids_rule_change(1)
            if not list_change_sensor:
                message = "No IDS rule on sensor changed"
                return utils.render_response_template(False, message), 400
                
            result = True
            fail_sensor = []
            for sensor in list_change_sensor:
                list_ids_rule = get_by_sensor_id_and_status(sensor['_id'], 'enable')
                content = {}
                content['sensor_ip'] = sensor['ip']
                content['action'] = 'post'
                content['url'] = '/ids'
                content['body'] = list_ids_rule
                post_result = utils.post_to_transport('/sensors/action', content)

                # post_result = post_to_transport('/ids', content)
                if not post_result:
                    result = False
                    fail_sensor.append(sensor['name'])
                else:
                    sensors_business.update_ids_rule_change(sensor['_id'], 0)

            if not result:
                message = message + " fail on " + str(fail_sensor)
                return utils.render_response_template(False, message), 400
                
            message = message + " success"
            return utils.render_response_template(True, message), 200
                
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500


@api.route("/disable")
# @admin_required
class IDSDisableRule(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(disable_model, validate=True)
    def post(self):
        """ Add item """
        try:
            data = utils.escape_request_with_model(request, add_model)

            data['status'] = 'disable'
            data['raw_content'] = None
            result = save_unique(data, 'rule_id')
            
            message = "Disable " + api_name
            if result:
                message = message + " success"
                return utils.render_response_template(True, message), 201
            else:
                message = message + " fail"
                return utils.render_response_template(False, message), 409
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500


    ### API: Get disable rule by sensor_name and rule_type
    @api.doc(params={
        'sensor_name': 'Sensor name',
        'type': 'IDS rule type'
    })
    def get(self):
        """ Get all item """
        try:
            sensor_name = request.args.get('sensor_name', type=str)
            _type = request.args.get('type', type=str)
            
            sensor = sensors_business.get_by_field(sensor_name, 'name')
            if not sensor:
                message = "Sensor not found"
                return utils.render_response_template(False, message), 404
            
            list_ids_rule = get_by_sensor_id_and_status_and_type(sensor['_id'], 'disable', _type)

            return list_ids_rule, 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = utils.render_response_template(False, message)
            return response_template, 500
