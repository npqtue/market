import traceback
import math
import time
from flask_pymongo import ObjectId
from pymongo import ReturnDocument

from app import mongo

if mongo:
    collection = mongo.db.endpoints
    col_endpoints_stats = mongo.db.endpoints_stats
    col_endpoints_devices = mongo.db.endpoints_devices

# Business to save data
def save(data):
    return collection.insert_one(data).inserted_id

def save_unique(data, unique_field):
    is_exist = collection.find_one({unique_field: data[unique_field]})
    if not is_exist:
        return collection.insert_one(data).inserted_id
    else:
        return False

# Business to update data
def update_by_id(_id, data):
    result = collection.update_one({"_id": ObjectId(_id)}, {"$set": data}, upsert=False).modified_count
    if not result:
        return False
    
    return True

# Business to delete data
def delete_by_id(_id):
    result = collection.delete_one({"_id": ObjectId(_id)}).deleted_count
    if not result:
        return False
    
    return True

# Function use by business
def to_dictionary(data):
    data['_id'] = str(data['_id'])
    return data


### Common use area (this area can copy to another business)
def get_all():
    documents = collection.find({})
    result = []
    for document in documents:
        result.append(to_dictionary(document))
    return result

def get_all_paging(page, limit):
    if not page or page <= 0:
        page = 1
    if not limit or limit <= 0:
        limit = 20
    documents = collection.find({}).sort([('_id', -1)]).skip((page-1)*limit).limit(limit)
    if documents:
        total_page = math.ceil(documents.count()/limit)
        result = {}
        result['records_per_page'] = documents.count(True)
        result['total_page'] = total_page
        result['total_items'] = documents.count()
        results = []
        for document in documents:
            document = to_dictionary(document)
            results.append(document)
        result['results'] = results
        
        return result
    else:
        return None

def get_by_id(_id):
    result = collection.find_one({"_id": ObjectId(_id)})
    if result:
        return to_dictionary(result)
    else:
        return None

def get_by_field(data, field_name):
    result = collection.find_one({field_name: data})
    if result:
        return to_dictionary(result)
    else:
        return None

### End of common area


### Another function below here
##############
### Get area
##############
def get_online_endpoints():
    shift_time = time.time() - 181
    return collection.find({'alive_time': {'$gt': shift_time}}).count()


def get_offline_endpoints():
    shift_time = time.time() - 181
    return collection.find({'alive_time': {'$lt': shift_time}}).count()

def get_stats(start_time, end_time):
    if not end_time:
        end_time = time.time()
    if not start_time:
        # If not start_time, get 1 days ago
        start_time = end_time - 86400

    items = col_endpoints_stats.find(
        {'$and': [
            {'time': {'$gte': start_time}},
            {'time': {'$lte': end_time}}
        ]}
    )

    if items.count() <= 0:
        return None
    
    result = []
    for item in items:
        result.append(to_dictionary(item))

    return result


##############
### Check area
##############


##############
### Update area
##############
def save_stats(data):
    try:
        return col_endpoints_stats.insert_one(data).inserted_id
    except:
        traceback.print_exc()
        return None

def save_plugable_event(data):
    try:
        return col_endpoints_devices.insert_one(data).inserted_id
    except:
        traceback.print_exc()
        return None

def update_plugable_event(data):
    try:
        result = col_endpoints_devices.find_one_and_update(
            {
                'endpoint_id': data['endpoint_id'], 
                'product_id': data['product_id']
            }, 
            {
                '$set': {'ejected_at': data['ejected_at']}
            }, sort=[('_id', -1)], return_document=ReturnDocument.AFTER
        )
        return to_dictionary(result)
    except:
        traceback.print_exc()
        return None


##############
### Delete area
##############


##############
### Another area
##############