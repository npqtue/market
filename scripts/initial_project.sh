#!/bin/bash
if [[ $0 != './initial_project.sh' ]]; then
    echo "Not in scripts folder, please cd to this folder and run again"
    exit 1
fi
if [[ $# != 1 ]]; then
    echo "Usage: $0 <project_name>"
    exit 1
fi

project_name=$1
cd ..
mv systemd.service ${project_name}.service
if [[ "$OSTYPE" == 'darwin'* ]]; then
    sed -i '' "s/template/$project_name/" ${project_name}.service
    sed -i '' "s/Template/$project_name/" config.py
else
    sed -i s:template:$project_name:g ${project_name}.service
    sed -i s:Template:$project_name:g config.py
fi