import math
import base64
from flask_pymongo import ObjectId

from app import mongo
if mongo:
    collection = mongo.db.sensors

# Business to save data
def save(data):
    return collection.insert_one(data)

def save_unique(data, unique_field):
    is_exist = collection.find_one({unique_field: data[unique_field]})
    if not is_exist:
        return collection.insert_one(data)
    else:
        return False

# Business to update data
def update_by_id(_id, data):
    is_exist = collection.find_one({"_id": ObjectId(_id)})
    if is_exist:
        return collection.update_one({"_id": ObjectId(_id)}, {"$set": data}, upsert=False)
    else:
        return False

# Business to delete data
def delete_by_id(_id):
    is_exist = collection.find_one({"_id": ObjectId(_id)})
    if is_exist:
        return collection.delete_one({"_id": ObjectId(_id)})
    else:
        return False

# Function use by business
def to_dictionary(data):
    data['_id'] = str(data['_id'])
    if 'crt_file' in data:
        data['crt_file'] = base64.b64decode(data['crt_file']).decode("utf-8")
    if 'key_file' in data:
        data['key_file'] = base64.b64decode(data['key_file']).decode("utf-8")
    if 'sslcert_file' in data:
        data['sslcert_file'] = base64.b64decode(data['sslcert_file']).decode("utf-8")
    return data


### Common use area (this area can copy to another business)
def get_all():
    documents = collection.find({})
    result = []
    for document in documents:
        result.append(to_dictionary(document))
    return result

def get_all_paging(page, limit):
    if not page or page <= 0:
        page = 1
    if not limit or limit <= 0:
        limit = 20
    documents = collection.find({}).sort([('_id', -1)]).skip((page-1)*limit).limit(limit)
    if documents:
        total_page = math.ceil(documents.count()/limit)
        result = {}
        result['records_per_page'] = documents.count(True)
        result['total_page'] = total_page
        result['total_items'] = documents.count()
        results = []
        for document in documents:
            document = to_dictionary(document)
            results.append(document)
        result['results'] = results
        
        return result
    else:
        return None

def get_by_id(_id):
    result = collection.find_one({"_id": ObjectId(_id)})
    if result:
        return to_dictionary(result)
    else:
        return None

def get_by_field(data, field_name):
    result = collection.find_one({field_name: data})
    if result:
        return to_dictionary(result)
    else:
        return None

### End of common area


### Another function below here
##############
### Get area
##############
def get_by_ids_rule_change(value):
    items = collection.find({'ids_rule_change': value})
    if items.count() == 0:
        return None
        
    result = []
    for item in items:
        result.append(to_dictionary(item))
    
    return result


def get_by_ips_rule_change(value):
    items = collection.find({'ips_rule_change': value})
    if items.count() == 0:
        return None
        
    result = []
    for item in items:
        result.append(to_dictionary(item))
    
    return result


##############
### Check area
##############


##############
### Update area
##############
def insert(data):
    data['_id'] = ObjectId(data['_id'])
    return collection.insert_one(data)


def update_ids_rule_change(_id, value):
    return collection.update_one(
        {'_id': ObjectId(_id)}, 
        {'$set': {'ids_rule_change': value}}
    )


def update_ips_rule_change(_id, value):
    return collection.update_one(
        {'_id': ObjectId(_id)}, 
        {'$set': {'ips_rule_change': value}}
    )
##############
### Delete area
##############


##############
### Another area
##############
def clear_sensity_info(data):
    del(data['token'])
    del(data['crt_file'])
    del(data['key_file'])
    del(data['sslcert_file'])
    del(data['ips_rule_change'])
    del(data['ids_rule_change'])
    return data