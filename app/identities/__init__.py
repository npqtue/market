import traceback
import time
import ipaddress
import geoip2.database
import IP2Location
import requests

from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, create_access_token
from flask_restplus import Namespace, Resource, fields

from app import app, logger, utils
from app.auth import auth_required, admin_required, staff_required
from .business import *
from app.sensors import business as sensors_business
from app.subnets import business as subnets_business
from app.endpoints import business as endpoints_business

api_name = 'Identities'

api = Namespace(api_name.lower(), description=api_name + ' api')

# >>>>>>>>>>>>>>>>>>>>>>>>>>> MODELS
# Nested template model
nested_schema = {}
nested_schema['field1'] = fields.String(required=False)
nested_schema['field2'] = fields.String(required=False)

# Template model
template_model = api.model(
    'Add ' + api_name + ' model',
    {
        'field_string': fields.String(required=True),
        'field_integer': fields.Integer(required=True),
        'field_list_string': fields.List(fields.String, required=True),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=True),
        'field_raw': fields.Raw(required=True)
    }
)

########################
# Collection structure
# {
#     'type',  # openstack-instance, user, server, network-device
#     'name',
#     'create_at',
#     'update_at',
#     'update_method', # auto or manual
#     'properties' {
#       'ip',
#       'os_type',
#       'project',  # cloud instance only
#       'subnet_id',
#       'vendor', # user, server and network device
#       'mac_addr',
#       'owner',  # cloud instance only
#     }
# }
##########################

# Edit below this line
add_model = api.model(
    'Add ' + api_name + ' model',
    {
        'type': fields.String(required=True),
        'name': fields.String(required=True),
        'properties': fields.Raw(required=True)
    }
)

edit_model = api.model(
    'Edit ' + api_name + ' model',
    {
        'type': fields.String(required=False),
        'name': fields.String(required=False),
        'update_method': fields.String(required=False),
        'properties': fields.Raw(required=False)
    }
)

# <<<<<<<<<<<<<<<<<<<<<<<<<<< ./END MODELS


@api.route("")
class Identities(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(add_model, validate=True)
    def post(self):
        """ Add item """
        try:
            data = utils.escape_request_with_model(request, add_model)
            data['create_at'] = time.time()
            data['update_at'] = time.time()
            data['update_method'] = 'manual'

            result = save(data)
            
            message = "Create " + api_name
            if result:
                message = message + " success"
                return utils.render_response_template(True, message), 201
            else:
                message = message + " fail"
                return utils.render_response_template(False, message), 400
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


    ### API: Get all item
    @api.doc(params={
        'page': 'Paging',
        'limit': 'Limit return records',
        'type': 'Type of identities'
    })
    def get(self):
        """ Get all item """
        try:
            page = request.args.get('page', type=int)
            limit = request.args.get('limit', type=int)
            _type = request.args.get('type', type=str)

            query = {}
            if _type:
                query['type'] = _type

            return get_all_with_query_paging(query, page, limit), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/<string:_id>")
class IdentitiesID(Resource):
    ### API: Get item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Get item by _id """
        try:
            result = get_by_id(_id)
            if not result:
                return utils.render_response_template(False, api_name + ' not found'), 404
            else:
                return result, 200
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500

    
    ### API: Edit item by _id
    @api.doc(params={
    })
    @api.expect(edit_model, validate=True)
    def put(self, _id):
        """ Edit item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            data = utils.escape_request_with_model(request, edit_model)
            data['update_at'] = time.time()
            data['update_method'] = 'manual'
            
            message = "Update " + api_name.lower()
            if not update_by_id(_id, data):
                message = message + " fail"
                return utils.render_response_template(False, message), 400
                
            message = message + " success"
            return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500

    ### API: Delete item by _id
    @api.doc(params={
    })
    def delete(self, _id):
        """ Delete item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            message = 'Delete ' + api_name.lower()
            if not delete_by_id(_id):
                message = message + ' fail'
                return utils.render_response_template(False, message), 400
            else:
                message = message + ' success'
                return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/<string:_type>/update")
class IdentitiesUpdateByType(Resource):
    ### API: Update by _type
    @api.doc(params={
    })
    def get(self, _type):
        """ Update by _type """
        try:
            message = 'Update identities'
            if _type not in ['openstack', 'nac', 'opnsense']:
                message = 'Identitiy type ' + _type + ' is not supported'
                return utils.render_response_template(False, message), 400

            result = False
            if _type == 'openstack':
                sensor = sensors_business.get_by_field('ht-openstack', 'name')
                content = {}
                content['sensor_ip'] = sensor['ip']
                content['url'] = '/identities?type=openstack'
                content['action'] = 'get'

                post_result = utils.post_to_transport('/sensors/action', content)

                if not post_result:
                    message = message + ' fail'
                    return utils.render_response_template(False, message), 400

                update_result = update_openstack_identities(post_result)
                if not update_result:
                    message = message + ' fail'
                    return utils.render_response_template(False, message), 400

                result = True

            if _type == 'nac':
                sensor = sensors_business.get_by_field('ht-nac', 'name')
                content = {}
                content['sensor_ip'] = sensor['ip']
                content['url'] = '/identities?type=nac'
                content['action'] = 'get'

                post_result = utils.post_to_transport('/sensors/action', content)

                if not post_result:
                    message = message + ' fail'
                    return utils.render_response_template(False, message), 400

                update_result = update_nac_identities(post_result)
                if not update_result:
                    message = message + ' fail'
                    return utils.render_response_template(False, message), 400

                result = True
            
            if not result:
                message = message + ' fail'
                return utils.render_response_template(False, message), 400

            message = message + ' success'
            return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/enrich/<string:_ip>")
class IdentitiesEnrichByIP(Resource):
    ### API: Enrich by _ip
    @api.doc(params={
    })
    def get(self, _ip):
        """ Enrich by _ip """
        try:
            message = 'Enrich identities'
            item = get_by_field(_ip, 'properties.ip')
            if item:
                subnet = subnets_business.get_by_ip(item['properties']['ip'])
                if subnet:
                    item['department'] = subnet
                return item, 200
            
            endpoint = endpoints_business.get_by_field(_ip, 'main_ip')
            if endpoint:
                identity = {}
                identity['name'] = endpoint['nick_name']
                identity['type'] = 'user'
                identity['properties'] = {}
                identity['properties']['ip'] = endpoint['main_ip']
                identity['properties']['os_type'] = endpoint['os_name']
                subnet = subnets_business.get_by_ip(endpoint['main_ip'])
                if subnet:
                    identity['department'] = subnet
                return identity, 200

            subnet = subnets_business.get_by_ip(_ip)
            if subnet:
                item = {}
                item['name'] = 'UNKNOWN'
                item['department'] = subnet
                return item, 200
            
            _ip = ipaddress.ip_address(_ip)
            enrich_ip = {}
            if _ip.is_global:
                try:
                    geolite_reader = geoip2.database.Reader(app.config['APP_DIR'] + '/GeoLite2-City.mmdb')
                    geolite = geolite_reader.city(str(_ip))

                    geopro_reader = IP2Location.IP2Location()
                    geopro_reader.open(app.config['APP_DIR'] + '/IP-COUNTRY-REGION-CITY-ISP.BIN')
                    geopro = geopro_reader.get_all(str(_ip))
                    if geopro.isp:
                        enrich_ip['name'] = geopro.isp
                    else:
                        enrich_ip['name'] = geopro.country_long
                    enrich_ip['flag'] = geopro.country_short
                    enrich_ip['location'] = True
                    enrich_ip['latitude'] = str(geolite.location.latitude)
                    enrich_ip['longitude'] = str(geolite.location.longitude)
                    geopro_reader.close()

                except:
                    traceback.print_exc()
                    pass

            content = {"loginKey": app.config['ADSP_KEY'], "ip": _ip}
            response = requests.post(app.config['ADSP_API'], data=content, verify=False)
            if response.text != 'false' and response.text != '':
                enrich_ip['name'] = response.text
                enrich_ip['is_adsp'] = True

            return enrich_ip, 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500