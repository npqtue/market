import math
import traceback
from flask_pymongo import ObjectId

from app import mongo, logger

if mongo:
    collection = mongo.db.alerts

# Business to save data
def save(data):
    return collection.insert_one(data).inserted_id

def save_unique(data, unique_field):
    is_exist = collection.find_one({unique_field: data[unique_field]})
    if not is_exist:
        return collection.insert_one(data).inserted_id
    else:
        return False

# Business to update data
def update_by_id(_id, data):
    result = collection.update_one({"_id": ObjectId(_id)}, {"$set": data}, upsert=False).modified_count
    if not result:
        return False
    
    return True

# Business to delete data
def delete_by_id(_id):
    result = collection.delete_one({"_id": ObjectId(_id)}).deleted_count
    if not result:
        return False
    
    return True

# Function use by business
def to_dictionary(data):
    data['_id'] = str(data['_id'])
    return data


### Common use area (this area can copy to another business)
def get_all():
    documents = collection.find({})
    result = []
    for document in documents:
        result.append(to_dictionary(document))
    return result

def get_all_paging(page, limit):
    if not page or page <= 0:
        page = 1
    if not limit or limit <= 0:
        limit = 20
    documents = collection.find({}).sort([('_id', -1)]).skip((page-1)*limit).limit(limit)
    if documents:
        total_page = math.ceil(documents.count()/limit)
        result = {}
        result['records_per_page'] = documents.count(True)
        result['total_page'] = total_page
        result['total_items'] = documents.count()
        results = []
        for document in documents:
            document = to_dictionary(document)
            results.append(document)
        result['results'] = results
        
        return result
    else:
        return None

def get_by_id(_id):
    result = collection.find_one({"_id": ObjectId(_id)})
    if result:
        return to_dictionary(result)
    else:
        return None

def get_by_field(data, field_name):
    result = collection.find_one({field_name: data})
    if result:
        return to_dictionary(result)
    else:
        return None

### End of common area


### Another function below here
##############
### Get area
##############


##############
### Check area
##############


##############
### Update area
##############


##############
### Delete area
##############


##############
### Another area
##############