# -*- coding: utf-8 -*-
# @Author: Toor
# @Date: 2019-05-02 14:37:37
# @Last Modified by:   Toor
# @Last Modified time: 2019-05-02 14:37:37

from os import environ

from app import create_app, v1_blueprint as apiv1
from config import config_dict

get_config_mode = environ.get('FLASK_CONFIG', 'local')
try:
    config_mode = config_dict[get_config_mode]
except KeyError:
    exit('Error: Invalid FLASK_CONFIG environment variable entry.')

app = create_app(config_mode)
app.register_blueprint(apiv1, url_prefix="/api/v1")

if __name__ == "__main__":
    app.run()
