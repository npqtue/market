import math
from flask_pymongo import ObjectId

from app import mongo

if mongo:
    collection = mongo.db.ips_rules
    from app.ips.aliases import business as aliases_business
    from app.sensors import business as sensors_business

# Business to save data
def save(data):
    return collection.insert_one(data)

def save_unique(data, unique_field):
    is_exist = collection.find_one({unique_field: data[unique_field]})
    if not is_exist:
        return collection.insert_one(data)
    else:
        return False

# Business to update data
def update_by_id(_id, data):
    is_exist = collection.find_one({"_id": ObjectId(_id)})
    if is_exist:
        return collection.update_one({"_id": ObjectId(_id)}, {"$set": data}, upsert=False)
    else:
        return False

# Business to delete data
def delete_by_id(_id):
    is_exist = collection.find_one({"_id": ObjectId(_id)})
    if is_exist:
        return collection.delete_one({"_id": ObjectId(_id)})
    else:
        return False

# Function use by business
def to_dictionary(data):
    data['_id'] = str(data['_id'])
    
    return data


### Common use area (this area can copy to another business)
def get_all():
    documents = collection.find({})
    result = []
    for document in documents:
        result.append(to_dictionary(document))
    return result

def get_all_paging(page, limit):
    if not page or page <= 0:
        page = 1
    if not limit or limit <= 0:
        limit = 20
    documents = collection.find({}).sort([('_id', -1)]).skip((page-1)*limit).limit(limit)
    if documents:
        total_page = math.ceil(documents.count()/limit)
        result = {}
        result['records_per_page'] = documents.count(True)
        result['total_page'] = total_page
        result['total_items'] = documents.count()
        results = []
        for document in documents:
            document = to_dictionary(document)
            fulfill_data(document)
            results.append(document)
        result['results'] = results
        
        return result
    else:
        return None

def get_by_id(_id):
    result = collection.find_one({"_id": ObjectId(_id)})
    if result:
        return to_dictionary(result)
    else:
        return None

def get_by_field(data, field_name):
    result = collection.find_one({field_name: data})
    if result:
        return to_dictionary(result)
    else:
        return None

### End of common area


### Another function below here
##############
### Get area
##############
def get_by_sensor_id(sensor_id):
    items = collection.find(
        {'sensor_id': sensor_id}
    )
    result = []
    for item in items:
        result.append(to_dictionary(item))
    
    return result


def get_by_alias_id(alias_id):
    items = collection.find(
        {'destination.ips_alias_id': alias_id}
    )
    result = []
    for item in items:
        result.append(to_dictionary(item))
    
    return result


##############
### Check area
##############


##############
### Update area
##############


##############
### Delete area
##############


##############
### Another area
##############
def fulfill_data(data):
    if 'ips_alias_id' in data['destination']:
        data['destination']['ips_alias'] = aliases_business.get_by_id(data['destination']['ips_alias_id'])
        del(data['destination']['ips_alias_id'])
        
    if 'sensor_id' in data:
        sensors = []
        for sensor_id in data['sensor_id']:
            sensor = sensors_business.get_by_id(sensor_id)
            sensors_business.clear_sensity_info(sensor)
            sensors.append(sensor)
        data['sensors'] = sensors
        del(data['sensor_id'])

    if 'any' in data['source']:
        data['source']['address'] = 'any'
        del(data['source']['any'])

    if 'any' in data['destination']:
        data['destination']['address'] = 'any'
        del(data['source']['any'])

    if 'port' not in data['source']:
        data['source']['port'] = 'any'

    if 'port' not in data['destination']:
        data['destination']['port'] = 'any'

    return data