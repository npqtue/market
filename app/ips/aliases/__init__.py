import traceback
import re

from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, create_access_token
from flask_restplus import Namespace, Resource, fields

from app import app, logger
from app.auth import auth_required, admin_required, staff_required
from app.utils import *
from .business import *
from app.ips.rules import business as ips_rules_business
from app.sensors import business as sensors_business

api_name = 'IPS Aliases'

api = Namespace('ips/aliases', description=api_name + ' api')

# >>>>>>>>>>>>>>>>>>>>>>>>>>> MODELS
# Nested template model
nested_schema = {}
nested_schema['field1'] = fields.String(required=False)
nested_schema['field2'] = fields.String(required=False)

# Template model
template_model = api.model(
    'Add ' + api_name + ' model',
    {
        'field_string': fields.String(required=True),
        'field_integer': fields.Integer(required=True),
        'field_list_string': fields.List(fields.String, required=True),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=True),
        'field_raw': fields.Raw(required=True)
    }
)

########################
# Collection structure
# {
#     'name',
#     'type', # network, host, domain or url
#     'value',
#     'description',  
# }
##########################

# Edit below this line
add_model = api.model(
    'Add ' + api_name + ' model',
    {
        'name': fields.String(required=True),
        'type': fields.String(required=True),
        'value': fields.List(fields.String, required=True),
        'description': fields.String(required=False)
    }
)

edit_model = api.model(
    'Edit ' + api_name + ' model',
    {
        'name': fields.String(required=False),
        'type': fields.String(required=False),
        'value': fields.List(fields.String, required=False),
        'description': fields.String(required=False)
    }
)

# <<<<<<<<<<<<<<<<<<<<<<<<<<< ./END MODELS


@api.route("")
# @admin_required
class IPSAliases(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(add_model, validate=True)
    def post(self):
        """ Add item """
        try:
            data = escape_request_with_model(request, add_model)

            # Bootstrap variable
            pattern = re.compile("^[a-zA-z0-9_]+$")
            if not pattern.match(data['name']):
                message = "Name is not allow space or special charactor"
                return render_response_template(False, message), 400

            result = save_unique(data, 'name')
            
            message = "Create " + api_name
            if result:
                message = message + " success"
                return render_response_template(True, message), 201
            else:
                message = message + " fail"
                return render_response_template(False, message), 409
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500


    ### API: Get all item
    @api.doc(params={
        'page': 'Paging',
        'limit': 'Limit return records'
    })
    def get(self):
        """ Get all item """
        try:
            page = request.args.get('page', type=int)
            limit = request.args.get('limit', type=int)

            return get_all_paging(page, limit), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500


@api.route("/<string:_id>")
# @admin_required
class IPSAliasesID(Resource):
    ### API: Get item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Get item by _id """
        try:
            result = get_by_id(_id)
            if not result:
                return render_response_template(False, api_name + ' not found'), 404
            else:
                return result, 200
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500

    
    ### API: Edit item by _id
    @api.doc(params={
    })
    @api.expect(edit_model, validate=True)
    def put(self, _id):
        """ Edit item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return render_response_template(False, api_name + ' not found'), 404

            data = escape_request_with_model(request, edit_model)
            message = "Update " + api_name.lower()
            if 'name' in data and data['name'] != item['name']:
                # Bootstrap variable
                pattern = re.compile("^[a-zA-z0-9_]+$")
                if not pattern.match(data['name']):
                    message = "Name is not allow space or special charactor"
                    return render_response_template(False, message), 400

                if get_by_field(data['name'], 'name'):
                    message = message + " fail"
                    return render_response_template(False, message), 409
            
            if not update_by_id(_id, data):
                message = message + " fail"
                return render_response_template(False, message), 400
            else:
                # When we change alias, it effect to ips_rule, so we must
                # update this rule to sensor.
                list_effect_ips_rule = ips_rules_business.get_by_alias_id(_id)
                if list_effect_ips_rule:
                    list_effect_sensor = []
                    for ips_rule in list_effect_ips_rule:
                        list_effect_sensor.extend(ips_rule['sensor_id'])
                    
                    if list_effect_sensor:
                        list_effect_sensor = set(list_effect_sensor)
                        for sensor_id in list_effect_sensor:
                            sensors_business.update_ips_rule_change(sensor_id, 1)

                message = message + " success"
                return render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500

    ### API: Delete item by _id
    @api.doc(params={
    })
    def delete(self, _id):
        """ Delete item by _id """
        try:
            if not get_by_id(_id):
                return render_response_template(False, api_name + ' not found'), 404

            constraint = check_constraint('ips_alias_id', _id)
            if not constraint:
                message = message + ' violate constraint'
                return render_response_template(False, message), 400

            message = 'Delete ' + api_name.lower()
            if not delete_by_id(_id):
                message = message + ' fail'
                return render_response_template(False, message), 400
            else:
                message = message + ' success'
                return render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            response_template = render_response_template(False, message)
            return response_template, 500