import traceback

from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, create_access_token
from flask_restplus import Namespace, Resource, fields

from app import app, logger, utils
from app.auth import auth_required, admin_required, staff_required
from .business import *

api_name = 'NAC switches'

api = Namespace('nac/switches', description=api_name + ' api')

# >>>>>>>>>>>>>>>>>>>>>>>>>>> MODELS
# Nested template model
nested_schema = {}
nested_schema['field1'] = fields.String(required=False)
nested_schema['field2'] = fields.String(required=False)

# Template model
template_model = api.model(
    'Add ' + api_name + ' model',
    {
        'field_string': fields.String(required=True),
        'field_integer': fields.Integer(required=True),
        'field_list_string': fields.List(fields.String, required=True),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=True),
        'field_raw': fields.Raw(required=True)
    }
)

########################
# Collection structure
# {
#     'field_string',
#     'field_integer',
#     'field_list_string',    
#     'field_nested',
#     'field_raw',
# }
##########################

# Edit below this line
add_model = api.model(
    'Add ' + api_name + ' model',
    {
        'field_string': fields.String(required=True),
        'field_integer': fields.Integer(required=True),
        'field_list_string': fields.List(fields.String, required=True),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=True),
        'field_raw': fields.Raw(required=True)
    }
)

edit_model = api.model(
    'Edit ' + api_name + ' model',
    {
        'field_string': fields.String(required=False),
        'field_integer': fields.Integer(required=False),
        'field_list_string': fields.List(fields.String, required=False),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=False),
        'field_raw': fields.Raw(required=False)
    }
)

# <<<<<<<<<<<<<<<<<<<<<<<<<<< ./END MODELS


@api.route("/interfaces")
class NACSwitchesInterfaces(Resource):
    ### API: Interactive with interface on switch
    @api.doc(params={
        'q': 'Query interface by parameters (ip:IP | ifname:INTERFACE_NAME | mac:MAC_ADDR)',
        'action': 'Action on interface'
    })
    def get(self):
        """ Interactive with interface on switch """
        try:
            message = "Action on " + api_name + " interface "
            query = request.args.get('q', type=str)
            action = request.args.get('action', type=str)

            if not query or not action:
                return utils.render_response_template(False, 'Query and action (?q=ip:127.0.0.1&action=disable) parameter is required'), 400
            
            if ':' not in query:
                return utils.render_response_template(False, 'Query string invalid (ip:IP | ifname:INTERFACE_NAME | mac:MAC_ADDR)'), 400

            content = {}
            content['sensor_ip'] = '10.0.103.12'
            content['action'] = 'get'
            content['url'] = request.full_path.replace('/api/v1','')

            post_result = utils.post_to_transport('/sensors/action', content)

            if not post_result:
                message = message + " fail"
                return utils.render_response_template(False, message), 400

            message = post_result['message']
            return post_result, 200
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500
