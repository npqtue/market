import traceback

import math, re
from flask_pymongo import ObjectId
import Crypto.Hash.MD5 as MD5

from app import mongo, redis

if mongo:
    collection = mongo.db.whitelists

# Business to save data
def save(data):
    return collection.insert_one(data).inserted_id

def save_unique(data, unique_field):
    is_exist = collection.find_one({unique_field: data[unique_field]})
    if not is_exist:
        return collection.insert_one(data).inserted_id
    else:
        return False

# Business to update data
def update_by_id(_id, data):
    result = collection.update_one({"_id": ObjectId(_id)}, {"$set": data}, upsert=False).modified_count
    if not result:
        return False
    
    return True

# Business to delete data
def delete_by_id(_id):
    result = collection.delete_one({"_id": ObjectId(_id)}).deleted_count
    if not result:
        return False
    
    return True

# Function use by business
def to_dictionary(data):
    data['_id'] = str(data['_id'])
    return data


### Common use area (this area can copy to another business)
def get_all():
    documents = collection.find({})
    result = []
    for document in documents:
        result.append(to_dictionary(document))
    return result

def get_all_paging(page, limit):
    if not page or page <= 0:
        page = 1
    if not limit or limit <= 0:
        limit = 20
    documents = collection.find({}).sort([('_id', -1)]).skip((page-1)*limit).limit(limit)
    if documents:
        total_page = math.ceil(documents.count()/limit)
        result = {}
        result['records_per_page'] = documents.count(True)
        result['total_page'] = total_page
        result['total_items'] = documents.count()
        results = []
        for document in documents:
            document = to_dictionary(document)
            results.append(document)
        result['results'] = results
        
        return result
    else:
        return None

def get_by_id(_id):
    result = collection.find_one({"_id": ObjectId(_id)})
    if result:
        return to_dictionary(result)
    else:
        return None

def get_by_field(data, field_name):
    result = collection.find_one({field_name: data})
    if result:
        return to_dictionary(result)
    else:
        return None

### End of common area


### Another function below here
##############
### Get area
##############


##############
### Check area
##############
def check(data):
    try:
        unique_alert_key = data['sensor_id'] + '-' + data['alert_name'] + '-' + data['src_ip'] + '-' + data['dest_ip']
        unique_alert_key = MD5.new(unique_alert_key.encode("utf-8")).digest()
        result = redis.get(unique_alert_key)
        if result:
            redis.set(unique_alert_key, '1', ex=300)
            return True
        else:
            redis.set(unique_alert_key, '1', ex=300)
    except:
        traceback.print_exc()
        pass

    items = collection.find({'sensor_id': data['sensor_id'], 'alert_name': data['alert_name']})
    if items.count() <= 0:
        return False

    is_whitelist = True
    for item in items:
        if 'endpoint_id' in data and item['endpoint_id'] != data['endpoint_id'] and item['endpoint_id'] != '*':
            is_whitelist = False

        if item['src_ip'] != data['src_ip'] and item['src_ip'] != '*':
            if '*' in item['src_ip']:
                pattern = re.compile(item['src_ip'])
                result = pattern.match(data['src_ip'])
                if not result:
                    is_whitelist = False
        if item['src_port'] != data['src_port'] and item['src_port'] != 0:
            is_whitelist = False
        
        if item['dest_ip'] != data['dest_ip'] and item['dest_ip'] != '*':
            if '*' in item['dest_ip']:
                pattern = re.compile(item['dest_ip'])
                result = pattern.match(data['dest_ip'])
                if not result:
                    is_whitelist = False
        if item['dest_port'] != data['dest_port'] and item['dest_port'] != 0:
            is_whitelist = False

        if item['proto'].upper() != data['proto'].upper() and item['proto'] != '*':
            is_whitelist = False
        
        if item['app_proto'].upper() != data['app_proto'].upper() and item['app_proto'] != '*':
            is_whitelist = False

    return is_whitelist


##############
### Update area
##############


##############
### Delete area
##############


##############
### Another area
##############