# -*- coding: utf-8 -*-  
# @Author: Toor  
# @Date: 2019-05-10 13:23:49  
# @Last Modified by:   Toor  
# @Last Modified time: 2019-05-10 13:23:49

from os import environ
import string, random
import Crypto.Hash.MD5 as MD5
from Crypto.PublicKey import RSA 
from Crypto.Cipher import PKCS1_OAEP
from Crypto import Random
import base64
import requests, json

from app import app, logger, mongo

def log_error(data):
    print(data)
    logger.error(data)


def escape_request_with_model(request, model):
    valid_properties = model._schema['properties']
    request_body = request.json
    data = {}
    for key in valid_properties.keys():
        if key in request_body:
            data[key] = request_body[key]

    return data


def check_constraint(field_name, value):
    all_collections = mongo.db.list_collection_names()
    if not all_collections:
        return False
    
    is_valid = True
    for collection in all_collections:
        if mongo.db[collection].find_one({field_name: value}):
            is_valid = False

    return is_valid


def gen_random_string(string_length):
    """Generate a random string with the combination of lowercase and uppercase letters """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(string_length))


def render_response_template(status, message):
    return {
        'status': status,
        'message': message
    }


def gen_authen_header():
    random_string = gen_random_string(16)
    hash_random_string = MD5.new(random_string.encode("utf-8")).digest()
    random_key = Random.new().read
    rsa_private_key = RSA.importKey(open(app.config['CORE_PRIKEY']).read())
    signed_string = rsa_private_key.sign(hash_random_string, random_key)[0]

    authen_string = base64.b64encode(json.dumps({
            'random_string': random_string,
            'signed_string': signed_string
        }).encode("utf-8"))

    headers={
        'Content-Type': 'application/json',
        'Authentication': authen_string
    }

    return headers


def post_to_transport(url, content):
    headers = gen_authen_header()
    response = requests.post(
        app.config['TRANSPORT_SERVER'] + url,
        data=json.dumps(content),
        headers=headers
    )

    if response.status_code == 200:
        return response.json()
    else:
        log_error(response.content)
        return None


def delete_to_transport(url):
    headers = gen_authen_header()
    response = requests.delete(
        app.config['TRANSPORT_SERVER'] + url,
        headers=headers
    )

    if response.status_code == 200:
        return response.json()
    else:
        log_error(response.content)
        return None