import traceback
import time

from flask import request
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, create_access_token
from flask_restplus import Namespace, Resource, fields

from app import app, logger, utils
from app.auth import auth_required, admin_required, staff_required
from .business import *
from app.sensors import business as sensors_business
from app.identities import business as identities_business
from app.subnets import business as subnets_business

api_name = 'Endpoints'

api = Namespace(api_name.lower(), description=api_name + ' api')

# >>>>>>>>>>>>>>>>>>>>>>>>>>> MODELS
# Nested template model
nested_schema = {}
nested_schema['field1'] = fields.String(required=False)
nested_schema['field2'] = fields.String(required=False)

# Template model
template_model = api.model(
    'Add ' + api_name + ' model',
    {
        'field_string': fields.String(required=True),
        'field_integer': fields.Integer(required=True),
        'field_list_string': fields.List(fields.String, required=True),
        'field_nested': fields.Nested(api.model('Nested model', nested_schema), required=True),
        'field_raw': fields.Raw(required=True)
    }
)

########################
# Collection structure
# {
#     'nick_name',
#     'mac_addrs_hash',
#     'cpu',    
#     'baseboard': {
#       'serial_number',
#       'model',
#       'manufacturer'
#     }
#     'disk_serial',
#     'access_token',
#     'os_name',
#     'os_version',
#     'config_interval': {
#       'applications',
#       'autorun',
#       'plugable-devices',
#       'connections',
#       'kernel',
#       'computers',
#       'processes',
#       'event-logs',
#       'active-devices'
#     }
#     'active_check_interval',
#     'create_time',
#     'update_time',
#     'active',
#     'register_pending',
#     'os_architect',
#     'version'
# }
##########################
# Edit below this line
add_model = api.model(
    'Add ' + api_name + ' model',
    {
        'sensor_name': fields.String(required=True),
        'nick_name': fields.String(required=True),
        'mac_addrs_hash': fields.String(required=True),
        'uuid': fields.String(required=True),
        'cpu': fields.String(required=False),
        'baseboard': fields.Raw(required=False),
        'disk_serial': fields.String(required=False),
        'os_name': fields.String(required=False),
        'os_version': fields.String(required=False),
        'os_architect': fields.String(required=False),
        'main_ip': fields.String(required=False),
        'register_pending': fields.Integer(required=True),
        'active': fields.Integer(required=True),
        'active_check_interval': fields.Integer(required=True),
        'config_interval': fields.Raw(required=False),
        'create_time': fields.Float(required=False),
        'update_time': fields.Float(required=False)
    }
)

edit_model = api.model(
    'Edit ' + api_name + ' model',
    {
        'register_pending': fields.Integer(required=False),
        'active': fields.Integer(required=False),
        'active_check_interval': fields.Integer(required=False),
        'config_interval': fields.Raw(required=False)
    }
)

event_model = api.model(
    'Edit ' + api_name + ' model',
    {
        
    }
)

# <<<<<<<<<<<<<<<<<<<<<<<<<<< ./END MODELS


@api.route("")
class Endpoints(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(add_model, validate=True)
    def post(self):
        """ Add item """
        try:
            data = utils.escape_request_with_model(request, add_model)

            # sensor = sensors_business.get_by_field(data['sensor_name'], 'name')
            # indentity = identities_business.get_by_type_and_name('user', data['nick_name'])
            subnet = subnets_business.get_by_ip(data['main_ip'])
            data['version'] = 0
            data['alive_time'] = time.time()

            if subnet:
                data['department'] = subnet['department']
                data['unit'] = subnet['unit']
                data['team'] = subnet['team']

            result = save(data)
            
            message = "Create " + api_name
            if result:
                message = message + " success"
                return utils.render_response_template(True, str(result)), 201
            else:
                message = message + " fail"
                return utils.render_response_template(False, message), 400
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


    ### API: Get all item
    @api.doc(params={
        'page': 'Paging',
        'limit': 'Limit return records'
    })
    def get(self):
        """ Get all item """
        try:
            page = request.args.get('page', type=int)
            limit = request.args.get('limit', type=int)

            return get_all_paging(page, limit), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/<string:_id>")
class EndpointsID(Resource):
    ### API: Get item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Get item by _id """
        try:
            result = get_by_id(_id)
            if not result:
                return utils.render_response_template(False, api_name + ' not found'), 404
            else:
                return result, 200
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500

    
    ### API: Edit item by _id
    @api.doc(params={
    })
    @api.expect(edit_model, validate=True)
    def put(self, _id):
        """ Edit item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            data = utils.escape_request_with_model(request, edit_model)
            message = "Update " + api_name.lower()

            sensor = sensors_business.get_by_field(item['sensor_name'], 'name')
            if not sensor:
                message = "Endpoint data is not valid, sensor not found"
                return utils.render_response_template(False, message), 400

            content = {}
            content['sensor_ip'] = sensor['ip']
            content['url'] = '/endpoints/' + item['_id']
            content['action'] = 'put'
            content['body'] = data
            post_result = utils.post_to_transport('/sensors/action', content)

            if not post_result:
                message = message + " fail"
                return utils.render_response_template(False, message), 400

            update_result = update_by_id(_id, data)
            if not update_result:
                message = message + " fail"
                return utils.render_response_template(False, message), 400

            message = message + " success"
            return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500

    ### API: Delete item by _id
    @api.doc(params={
    })
    def delete(self, _id):
        """ Delete item by _id """
        try:
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            message = 'Delete ' + api_name.lower()
            sensor = sensors_business.get_by_field(item['sensor_name'], 'name')
            if not sensor:
                message = "Endpoint data is not valid, sensor not found"
                return utils.render_response_template(False, message), 400

            content = {}
            content['sensor_ip'] = sensor['ip']
            content['url'] = '/endpoints/' + item['_id']
            content['action'] = 'delete'
            post_result = utils.post_to_transport('/sensors/action', content)

            if not post_result:
                message = message + " fail"
                return utils.render_response_template(False, message), 400

            if not delete_by_id(_id):
                message = message + ' fail'
                return utils.render_response_template(False, message), 400
            else:
                message = message + ' success'
                return utils.render_response_template(True, message), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500



@api.route("/<string:_id>/ping")
class EndpointsIDPing(Resource):
    ### API: Keepalive item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Keepalive item by _id """
        try:
            result = get_by_id(_id)
            if not result:
                return utils.render_response_template(False, api_name + ' not found'), 404
            else:
                version = request.args.get('version', type=str)
                if not version:
                    version = '0'
                data = {'alive_time': time.time(), 'version': version}
                update_by_id(_id, data)
                return 'pong', 200
                
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/<string:_id>/approve")
class EndpointsIDApprove(Resource):
    ### API: Approve item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Approve item by _id """
        try:
            message = 'Approve ' + api_name.lower()
            item = get_by_id(_id)
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            if item['register_pending'] == 0:
                return utils.render_response_template(False, api_name + ' already approved'), 400 

            sensor = sensors_business.get_by_field(item['sensor_name'], 'name')
            if not sensor:
                message = "Endpoint data is not valid, sensor not found"
                return utils.render_response_template(False, message), 400
            
            data = {'register_pending': 0}
            content = {}
            content['sensor_ip'] = sensor['ip']
            content['url'] = '/endpoints/' + item['_id']
            content['action'] = 'put'
            content['body'] = data
            post_result = utils.post_to_transport('/sensors/action', content)

            if not post_result:
                message = message + " fail"
                return utils.render_response_template(False, message), 400

            update_result = update_by_id(_id, data)
            if not update_result:
                message = message + " fail"
                return utils.render_response_template(False, message), 400

            message = message + " success"
            return utils.render_response_template(True, message), 200
                
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/status")
class EndpointsStatus(Resource):
    ### API: Check endpoint status
    @api.doc(params={
        'ip': 'Check status by endpoint IP'
    })
    def get(self):
        """ Check endpoint status """
        try:
            message = 'Get status of ' + api_name.lower()
            _ip = request.args.get('ip', type=str)
            item = get_by_field(_ip, 'main_ip')
            if not item:
                return utils.render_response_template(False, api_name + ' not found'), 404

            status = {}
            if 'alive_time' in item and item['alive_time'] >= time.time() - 181:
                status['online'] = True
            else:
                status['online'] = False

            return status, 200
                
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/calculate_stats")
class EndpointsCalculateStats(Resource):
    ### API: Calculate endpoints stats
    @api.doc(params={
    })
    def get(self):
        try:
            message = 'Calculate stats of ' + api_name.lower()
            stats = {}
            stats['online_count'] = get_online_endpoints()
            stats['offline_count'] = get_offline_endpoints()
            stats['time'] = time.time()
            print(stats)

            result = save_stats(stats)
            if not result:
                message = message + " fail"
                return utils.render_response_template(False, message), 400

            del(stats['_id'])
            return stats, 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/stats")
class EndpointsStats(Resource):
    ### API: Calculate endpoints stats
    @api.doc(params={
    })
    def get(self):
        try:
            message = 'Get stats of ' + api_name.lower()
            start_time = request.args.get('start_time', type=float)
            end_time = request.args.get('end_time', type=float)
            result = get_stats(start_time, end_time)
            if not result:
                message = message + " fail"
                return utils.render_response_template(False, message), 400

            return result, 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/plugable-events")
class EndpointsPlugableEvent(Resource):
    ### API: Add item
    @api.doc(params={
    })
    @api.expect(event_model, validate=True)
    def post(self):
        """ Add item """
        try:
            message = "Add events of " + api_name
            data = request.json

            result = None
            if 'injected_at' in data:
                result = save_plugable_event(data)
                if not result:
                    message = message + " fail"
                    return utils.render_response_template(False, message), 400
                message = message + " success"
                return utils.render_response_template(True, str(result)), 201

            elif 'ejected_at' in data:
                result = update_plugable_event(data)
                if not result:
                    message = message + " fail"
                    return utils.render_response_template(False, message), 400
                
                return result, 200
            
        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500


@api.route("/<string:_id>/update_department")
class EndpointsIDUpdateDeparment(Resource):
    ### API: Approve item by _id
    @api.doc(params={
    })
    def get(self, _id):
        """ Approve item by _id """
        try:
            from app import mongo
            endpoints = mongo.db.endpoints.find()
            for endpoint in endpoints:
                subnet = subnets_business.get_by_ip(endpoint['main_ip'])

                if subnet:
                    update_data = {}
                    update_data['department'] = subnet['department']
                    update_data['unit'] = subnet['unit']
                    update_data['team'] = subnet['team']

                    update_by_id(endpoint['_id'], update_data)
          
            return utils.render_response_template(True, "success"), 200

        except:
            traceback.print_exc()
            logger.exception('Exception: ', exc_info=True)
            message = "Internal server error"
            return utils.render_response_template(False, message), 500
