# -*- coding: utf-8 -*-
# @Author: Toor
# @Date: 2019-05-02 16:44:39
# @Last Modified by:   Toor
# @Last Modified time: 2019-05-02 16:44:39

import os


class Config(object):
    # Basic settings
    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    APP_NAME = 'core'
    JWT_SECRET_KEY = 'This-Is-Very-Secrect-Key'
    JWT_ACCESS_TOKEN_EXPIRES = False

    # Customize settings
    USING_MONGO = True
    USING_ES = False
    USING_REDIS = True
    USING_RABBITMQ = False

    # Application settings
    MONGODB_NAME = 'core'
    # Private key for talking with transport service
    CORE_PRIKEY = APP_DIR + '/.key/core_private_key.pem'
    # Database ORM:
    # SQLALCHEMY_DATABASE_URI = 'postgres://lebron:thumbsup196@localhost:5432/lebrondb'
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:postgres@localhost:5432/dblebron'



class TestConfig(Config):
    DEBUG = True
    if Config.USING_MONGO is True:
        MONGO_URI = 'mongodb://mongo-test:27017/' + Config.MONGODB_NAME + '_test'

    if Config.USING_ES is True:
        ES_HOST = ['elasticsearch-test']
        ES_USER = 'index_manager'
        ES_PASSWORD = 'kPD73Uh8SxyCXLCsPRQMWzZhP'
        ES_SCHEME = 'http'
        ES_PORT = '9200'

    if Config.USING_REDIS is True:
        REDIS_HOST = 'redis-test'
        REDIS_PORT = '6379'

    if Config.USING_RABBITMQ is True:
        QUEUE_HOST = 'rabbitmq-test'
        QUEUE_USER = 'soc'
        QUEUE_VHOST = '/soc'
        QUEUE_PASSWORD = 'dev_password'
        QUEUE_NAME = 'alerts'

    # Another config below here
    TRANSPORT_SERVER = 'http://127.0.0.1:8081/api/v1'
    ADSP_API = 'https://ads.systemprojects.net/adsp/API/Server'
    ADSP_KEY = '2GKkusXrisywUKImJjhF6/jiQNodscS2oaTscBnf3jt0p4eplgLnyqmjO0qwr1TWkHUmQMmCL6c1fxPHq27QSa8YzUj06CSXqWbue6JPwQY='


class LocalConfig(Config):
    DEBUG = True
    if Config.USING_MONGO is True:
        MONGO_URI = 'mongodb://172.16.233.226:27017/' + Config.MONGODB_NAME + '_local'

    if Config.USING_ES is True:
        ES_HOST = ['elasticsearch-local']
        ES_USER = 'index_manager'
        ES_PASSWORD = 'kPD73Uh8SxyCXLCsPRQMWzZhP'
        ES_SCHEME = 'http'
        ES_PORT = '9200'

    if Config.USING_REDIS is True:
        REDIS_HOST = 'redis-local'
        REDIS_PORT = '6379'

    if Config.USING_RABBITMQ is True:
        QUEUE_HOST = 'rabbitmq-local'
        QUEUE_USER = 'soc'
        QUEUE_VHOST = '/soc'
        QUEUE_PASSWORD = 'dev_password'
        QUEUE_NAME = 'alerts'

    # Another config below here
    TRANSPORT_SERVER = 'http://127.0.0.1:8081/api/v1'
    ADSP_API = 'https://ads.systemprojects.net/adsp/API/Server'
    ADSP_KEY = '2GKkusXrisywUKImJjhF6/jiQNodscS2oaTscBnf3jt0p4eplgLnyqmjO0qwr1TWkHUmQMmCL6c1fxPHq27QSa8YzUj06CSXqWbue6JPwQY='


class DevConfig(Config):
    DEBUG = True
    if Config.USING_MONGO is True:
        MONGO_URI = 'mongodb://127.0.0.1:27017/' + Config.MONGODB_NAME

    if Config.USING_ES is True:
        ES_HOST = ['elasticsearch-dev']
        ES_USER = 'index_manager'
        ES_PASSWORD = 'kPD73Uh8SxyCXLCsPRQMWzZhP'
        ES_SCHEME = 'http'
        ES_PORT = '9200'

    if Config.USING_REDIS is True:
        REDIS_HOST = 'redis-dev'
        REDIS_PORT = '6379'

    if Config.USING_RABBITMQ is True:
        QUEUE_HOST = 'rabbitmq-dev'
        QUEUE_USER = 'soc'
        QUEUE_VHOST = '/soc'
        QUEUE_PASSWORD = 'dev_password'
        QUEUE_NAME = 'alerts'

    # Another config below here
    TRANSPORT_SERVER = 'http://172.16.233.220:8081/api/v1'
    ADSP_API = 'https://ads.systemprojects.net/adsp/API/Server'
    ADSP_KEY = '2GKkusXrisywUKImJjhF6/jiQNodscS2oaTscBnf3jt0p4eplgLnyqmjO0qwr1TWkHUmQMmCL6c1fxPHq27QSa8YzUj06CSXqWbue6JPwQY='


class ProdConfig(Config):
    DEBUG = False
    if Config.USING_MONGO is True:
        MONGO_URI = 'mongodb://127.0.0.1:27017/' + Config.MONGODB_NAME

    if Config.USING_ES is True:
        ES_HOST = ['elasticsearch-prod']
        ES_USER = 'index_manager'
        ES_PASSWORD = 'kPD73Uh8SxyCXLCsPRQMWzZhP'
        ES_SCHEME = 'http'
        ES_PORT = '9200'

    if Config.USING_REDIS is True:
        REDIS_HOST = '127.0.0.1'
        REDIS_PORT = '6379'

    if Config.USING_RABBITMQ is True:
        QUEUE_HOST = 'rabbitmq-prod'
        QUEUE_USER = 'soc'
        QUEUE_VHOST = '/soc'
        QUEUE_PASSWORD = 'dev_password'
        QUEUE_NAME = 'alerts'

    # Another config below here
    TRANSPORT_SERVER = 'https://soc-transport.systemprojects.net/api/v1'
    ADSP_API = 'https://ads.systemprojects.net/adsp/API/Server'
    ADSP_KEY = '2GKkusXrisywUKImJjhF6/jiQNodscS2oaTscBnf3jt0p4eplgLnyqmjO0qwr1TWkHUmQMmCL6c1fxPHq27QSa8YzUj06CSXqWbue6JPwQY='


config_dict = {
    'test': TestConfig,
    'local': LocalConfig,
    'dev': DevConfig,
    'prod': ProdConfig
}
