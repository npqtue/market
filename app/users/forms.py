from flask import jsonify
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields import SelectField
# from app.conf.models import Department, Role
from wtforms.validators import DataRequired


def get_departments():
    output = []
    names = Department.query.all()
    for name in names:
        output.append(name.name)
    return names


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


class CreateUserForm(FlaskForm):
    username = StringField('Username')
    fullname = StringField('Fullname')
    email = StringField('Email')
    password = PasswordField('Password')
    description = StringField('Description')

    #role = QuerySelectField("Role",
    #                         query_factory=lambda: Role.query.all(),
    #                         get_label="description")


    #departments = QuerySelectField("Departments",
    #                               query_factory=get_departments,
    #                               get_label="name", allow_blank=False, id='user_departments')
    #teams = QuerySelectField("Teams",
    #                         query_factory=lambda: Department.query.all(),
    #                         get_label="team", allow_blank=False, id='user_teams')

    #departments = SelectField('Departments')
    #departments = StringField('Departments', id='user_departments')