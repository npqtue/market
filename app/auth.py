# -*- coding: utf-8 -*-
# @Author: Shockwave
# @Date:   2019-04-05 23:06:59
# @Last Modified by:   Shockwave
# @Last Modified time: 2019-04-06 00:22:15


from functools import wraps
from flask import abort
from flask_jwt_extended import (
    verify_jwt_in_request,
    get_jwt_claims
)

from flask_jwt_extended import jwt_required
from flask_jwt_extended.view_decorators import _decode_jwt_from_request
from flask_jwt_extended.exceptions import NoAuthorizationError


#--------------------------- Any authenticated ---
def auth_required(view_function):
    @wraps(view_function)
    def wrapper(*args, **kwargs):
        try:
            jwt_data = _decode_jwt_from_request(request_type='access')

            if jwt_data:
                authorized = True
            else:
                authorized = False

            if not authorized:
                abort(403)
        except:
            abort(403)

        return view_function(*args, **kwargs)

    return jwt_required(wrapper)


#--------------------------- Register role ---
def register_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()

        if claims['role'] != 'register':
            abort(403)
        else:
            return fn(*args, **kwargs)
    return wrapper


#--------------------------- Staff role ---
def staff_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()

        if claims['role'] != 'staff':
            abort(403)
        else:
            return fn(*args, **kwargs)
    return wrapper


#--------------------------- Manager role ---
def manager_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()

        if claims['role'] != 'manager':
            abort(403)
        else:
            return fn(*args, **kwargs)
    return wrapper


#--------------------------- Admin role ---
def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()

        if claims['role'] != 'admin':
            abort(403)
        else:
            return fn(*args, **kwargs)
    return wrapper
